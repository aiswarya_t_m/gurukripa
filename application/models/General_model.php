<?php
class General_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
   public function form_register_insert($data){
       $this->db->insert('tbl_registration',$data);

       $insert_id = $this->db->insert_id();

       $webid = "GR".sprintf("%'.05d\n",$insert_id);

       $data1 = array(
           
           'web_id' => $webid,

       );

       $this->db->where('id', $insert_id);
     return $this->db->update('tbl_registration', $data1);
        
    }
   public function login($data) {

        $condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "'";
        $this->db->select('*');
        $this->db->from('tbl_registration');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    // Read data from database to show data in admin page
    public function read_user_information($username) {

        $condition = "username =" . "'" . $username . "'";
        $this->db->select('*');
        $this->db->from('tbl_registration');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function get_states(){

    $this->db->select("*");
    $this->db->from("tbl_states");
    return $this->db->get()->result_array();

    }

    public function get_featured(){

    $this->db->select("*");
    $this->db->from("tbl_registration");
    $this->db->where("payment",0);
    $this->db->where("role",'User');
    $this->db->limit(6);
    return $this->db->get()->result_array();


    }
  

}
?>
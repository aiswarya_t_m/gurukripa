<?php
class Admin_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

     public function religion_list(){
    $this->db->select('*');
    $this->db->from('tbl_religion');
    $query=$this->db->get();
    return $query->result_array();
  }
 

   
   public function get_religion($id){

    $this->db->select("*");
    $this->db->from("tbl_religion");
    $this->db->where("id",$id);
    return $this->db->get()->row();

   }

   public function form_religion_insert($data){

     return $this->db->insert("tbl_religion",$data);

   }

   public function form_religion_edit($data1,$id){

     $this->db->where('id', $id);
     return $this->db->update('tbl_religion', $data1);

   }

   public function religion_delete($id){

     $this->db->where('id', $id);
     return $this->db->delete('tbl_religion');

   }
   public function form_caste_insert($data){

   return $this->db->insert("tbl_caste",$data);
   }

    public function form_caste_edit($data1,$id){

     $this->db->where('id', $id);
     return $this->db->update('tbl_caste', $data1);

   }

   public function get_caste($id){

    $this->db->select("*");
    $this->db->from("tbl_caste");
    $this->db->where("id",$id);
    return $this->db->get()->row();

   }

   public function caste_delete($id){

     $this->db->where('id', $id);
     return $this->db->delete('tbl_caste');

   }

   public function caste_list(){
    $this->db->select('*,tbl_caste.id as id,tbl_caste.name as c_name,tbl_religion.id as rid,tbl_religion.name as religion');
    $this->db->from('tbl_caste');
    $this->db->join('tbl_religion','tbl_religion.id = tbl_caste.religion_id');
    $query=$this->db->get();
    return $query->result_array();
  }


  public function form_package_insert($data){

   return $this->db->insert("tbl_package",$data);
   }

    public function form_package_edit($data1,$id){

     $this->db->where('id', $id);
     return $this->db->update('tbl_package', $data1);

   }

   public function get_package($id){

    $this->db->select("*");
    $this->db->from("tbl_package");
    $this->db->where("id",$id);
    return $this->db->get()->row();

   }

   public function package_delete($id){

     $this->db->where('id', $id);
     return $this->db->delete('tbl_package');

   }

   public function package_list(){
    $this->db->select('*');
    $this->db->from('tbl_package');
    $query=$this->db->get();
    return $query->result_array();
  }

  public function get_users(){

    $this->db->select('*');
    $this->db->from('tbl_registration');
    $query=$this->db->get();
    return $query->result_array();


  }

  public function get_paid_users(){

    $this->db->select('*');
    $this->db->from('tbl_registration');
    $this->db->where("payment",1);
    $query=$this->db->get();
    return $query->result_array();


  }
  

   public function get_user_details($id){

    $this->db->select('*');
    $this->db->from('tbl_registration');
     $this->db->where('id', $id);
    $query=$this->db->get();
    return $query->row();


  }

  
}
?>
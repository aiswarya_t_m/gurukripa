<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userhome extends CI_Controller {

    public function __construct()
    {

        parent::__construct();
        if (!(isset($this->session->userdata['user_logged_in']))) {
            redirect(base_url());
        }
       $this->load->model('General_model','general');
    }
    public function index(){

        $data['results']=  $this->general->categorylist();
        $data['no_of_users'] = $this->general->get_no_users();
        $this->load->view('english/home_page',$data);   

    }
   public function user_profile($id){
        $data['results']=  $this->general->profile($id);
        $this->load->view('english/profile',$data);  
   }
   
   public function change_profile(){
         
      if(!empty($_FILES['picture']['name'])){
                        $config['upload_path'] = 'assets/profiles/';
                        $config['allowed_types'] = 'gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp';
                        $config['file_name'] = $_FILES['picture']['name'];

                        //Load upload library and initialize configuration
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);

                        if($this->upload->do_upload('picture')){
                            $uploadData = $this->upload->data();
                            $picture = $uploadData['file_name'];
                        }else{
                            $picture = '';
                        }
                    }else{
                        $picture = '';
                    }
        
        $dat = array(
            'profile_picture' => $picture,
        );
        $sesn = $this->session->userdata['user_logged_in'];   
        $user_id = $sesn['user_id'];

        $update=$this->general->update_profile($user_id,$dat);
     
        echo json_encode(array('result'=>'success'));
   }
   public function place_updation(){
     $sesn = $this->session->userdata['user_logged_in'];   
     $user_id = $sesn['user_id'];

     $data = array(
      'place' => $this->input->post('place'),
     );
      if($this->general->update_profile($user_id,$data))
        {

            $data['results']=  $this->general->profile($user_id);
            $this->load->view('english/profile',$data); 

        }
        else
        {

           $data['results']=  $this->general->profile($user_id);
           $this->load->view('english/profile',$data); 
        }
   }
   public function home_town_updation(){
     $sesn = $this->session->userdata['user_logged_in'];   
     $user_id = $sesn['user_id'];

     $data = array(
      'home_town' => $this->input->post('home_town'),
     );
      if($this->general->update_profile($user_id,$data))
        {

            $data['results']=  $this->general->profile($user_id);
            $this->load->view('english/profile',$data); 

        }
        else
        {

           $data['results']=  $this->general->profile($user_id);
           $this->load->view('english/profile',$data); 
        }
   }
   public function about_updation(){
     $sesn = $this->session->userdata['user_logged_in'];   
     $user_id = $sesn['user_id'];

     $data = array(
      'full_name' => $this->input->post('full_name'),
      'bio' => $this->input->post('bio'),
      'about' => $this->input->post('about'),
     );
      if($this->general->update_profile($user_id,$data))
        {

            $data['results']=  $this->general->profile($user_id);
            $this->load->view('english/profile',$data); 

        }
        else
        {

           $data['results']=  $this->general->profile($user_id);
           $this->load->view('english/profile',$data); 
        }
   }
   public function work_experience_updation(){
     $sesn = $this->session->userdata['user_logged_in'];   
     $user_id = $sesn['user_id'];
     if(isset($_POST['sbt']))
        {
          $this->db->select('*');
          $this->db->from('tbl_work_experience');
          $this->db->where('user_id',$user_id);
          $query=$this->db->get();
          $result =  $query->result_array();
          if(empty($result)){
                $data = $this->input->post();
                $company = $data['company'];
                $position = $data['position'];
                $start_month = $data['start_month'];
                $start_year = $data['start_year'];
                $end_month = $data['end_month'];
                $end_year = $data['end_year'];
               
                if (isset($data['continue_in_office'])) {
             
                  $continue_in_office = 1;
                }
                else{
        
                    $continue_in_office = 0;
                }

                $cnt = count($company);
           
                for ($i=0; $i < $cnt; $i++) {
                    if($company[$i]){
                        $dta = array(
                            'user_id' => $user_id,
                            'company' => $company[$i],
                            'position' => $position[$i],
                            'start_month' => $start_month[$i],
                            'start_year' => $start_year[$i],
                            'end_month' => $end_month[$i],
                            'end_year' => $end_year[$i],
                            'continue_in_office' => $continue_in_office,
                        );
                        $this->db->insert('tbl_work_experience',$dta);
                    }
                }
          }
          else{
            $this -> db -> where('tbl_work_experience.user_id', $user_id);
            $this -> db -> delete('tbl_work_experience');

                $data = $this->input->post();
                $company = $data['company'];
                $position = $data['position'];
                $start_month = $data['start_month'];
                $start_year = $data['start_year'];
                $end_month = $data['end_month'];
                $end_year = $data['end_year'];
                if (isset($data['continue_in_office'])) {
             
                  $continue_in_office = 1;
                }
                else{
        
                    $continue_in_office = 0;
                }
                $cnt = count($company);
           
                for ($i=0; $i < $cnt; $i++) {
                    if($company[$i]){
                        $dta = array(
                            'user_id' => $user_id,
                            'company' => $company[$i],
                            'position' => $position[$i],
                            'start_month' => $start_month[$i],
                            'start_year' => $start_year[$i],
                            'end_month' => $end_month[$i],
                            'end_year' => $end_year[$i],
                            'continue_in_office' => $continue_in_office,
                        );
                        $this->db->insert('tbl_work_experience',$dta);
                    }
                }
          }

}


        
       $this->user_profile($user_id);
   }
   public function education_updation(){
     $sesn = $this->session->userdata['user_logged_in'];   
     $user_id = $sesn['user_id'];
     if(isset($_POST['sbt_e']))
        {
          $this->db->select('*');
          $this->db->from('tbl_education');
          $this->db->where('user_id',$user_id);
          $query=$this->db->get();
          $result =  $query->result_array();
          if(empty($result)){
                $data = $this->input->post();
                $name_of_institute = $data['name_of_institute'];
                $course = $data['course'];
                $subject = $data['subject'];
                $start_month = $data['start_month'];
                $start_year = $data['start_year'];
                $end_month = $data['end_month'];
                $end_year = $data['end_year'];
                $cnt = count($name_of_institute);

                if (isset($data['now_continue'])) {
             
                  $now_continue = 1;
                }
                else{
        
                    $now_continue = 0;
                }
           
                for ($i=0; $i < $cnt; $i++) {
                    if($name_of_institute[$i]){
                        $dta = array(
                            'user_id' => $user_id,
                            'name_of_institute' => $name_of_institute[$i],
                            'course' => $course[$i],
                            'subject' => $subject[$i],
                            'start_month' => $start_month[$i],
                            'start_year' => $start_year[$i],
                            'end_month' => $end_month[$i],
                            'end_year' => $end_year[$i],
                            'now_continue' => $now_continue,
                        );
                        $this->db->insert('tbl_education',$dta);
                    }
                }
          }
          else{
            $this -> db -> where('tbl_education.user_id', $user_id);
            $this -> db -> delete('tbl_education');

                $data = $this->input->post();
                $name_of_institute = $data['name_of_institute'];
                $course = $data['course'];
                $subject = $data['subject'];
                $start_month = $data['start_month'];
                $start_year = $data['start_year'];
                $end_month = $data['end_month'];
                $end_year = $data['end_year'];
                
               if (isset($data['now_continue'])) {
             
                  $now_continue = 1;
                }
                else{
        
                    $now_continue = 0;
                }

                $cnt = count($name_of_institute);
           
                for ($i=0; $i < $cnt; $i++) {
                    if($name_of_institute[$i]){
                        $dta = array(
                            'user_id' => $user_id,
                            'name_of_institute' => $name_of_institute[$i],
                            'course' => $course[$i],
                            'subject' => $subject[$i],
                            'start_month' => $start_month[$i],
                            'start_year' => $start_year[$i],
                            'end_month' => $end_month[$i],
                            'end_year' => $end_year[$i],
                            'now_continue' => $now_continue,
                        );
                        $this->db->insert('tbl_education',$dta);
                    }
                }
          }

}
       $this->user_profile($user_id);
   }
   public function add_question(){
     $sesn = $this->session->userdata['user_logged_in'];   
     $user_id = $sesn['user_id'];
    if(isset($_POST['sbt'])){
    $dta = array(
              'question' => $this->input->post('question'),
              'user_id' => $user_id 
          );
    
       if($insert_id = $this->general->add_question_and_categories($dta))
        {
            $get_admin = $this->general->get_admin();
            $user_name =  $this->general->get_user($user_id);
            $msg = $user_name." "."Added New Question";
            $data_notification = array(
                      'user_id' => $user_id,
                      'msg' => $msg,
                      'q_id' => $insert_id,
                      'to_user_id' => $get_admin,
                      'status' =>1
                  );
            $this->db->insert('tbl_notifications',$data_notification);

            $data['results']=  $this->general->profile($user_id);
            $this->load->view('english/profile',$data); 

        }
        else
        {

           $data['results']=  $this->general->profile($user_id);
           $this->load->view('english/profile',$data); 
        }
    }
    else{
    $data['results']=  $this->general->categorylist();
    $this->load->view('english/add_question',$data);
    }
   }
   public function add_post(){
     $sesn = $this->session->userdata['user_logged_in'];   
     $user_id = $sesn['user_id'];
    if(isset($_POST['sbt'])){
    $dta = array(
              'user_id' => $user_id,
              'post_title' => $this->input->post('post_title'), 
              'post_content' => $this->input->post('post_content')
          );
   
       if($insert_id = $this->general->add_post($dta))
        {
          $get_admin = $this->general->get_admin();
          $user_name =  $this->general->get_user($user_id);
          $msg = $user_name." "."Added New Post";
          $data_notification = array(
                    'user_id' => $user_id,
                    'msg' => $msg,
                    'p_id' => $insert_id,
                    'to_user_id' => $get_admin,
                    'status' =>1
                );
          $this->db->insert('tbl_notifications',$data_notification);

            $data['results']=  $this->general->profile($user_id);
            $this->load->view('english/profile',$data); 

        }
        else
        {

           $data['results']=  $this->general->profile($user_id);
           $this->load->view('english/profile',$data); 
        }
    }
    else{
    $data['results']=  $this->general->categorylist();
    $this->load->view('english/add_post',$data);
    }
   }
   public function follow_now($id){
     $sesn = $this->session->userdata['user_logged_in'];   
     $user_id = $sesn['user_id'];

     $dta = array(
      'followers' =>$user_id,
      'following' =>$id
     );
    $this->db->insert('tbl_follow',$dta);

        $data['results'] =  $this->general->profile($id);
        $data['user_id'] = $id; 
        $this->load->view('english/public_profile',$data);
   }
   public function unfollow_now($id){
     $sesn = $this->session->userdata['user_logged_in'];   
     $user_id = $sesn['user_id'];
     $this -> db -> where('tbl_follow.followers', $user_id);
     $this -> db -> where('tbl_follow.following', $id);
     $this -> db -> delete('tbl_follow');

        $data['results'] =  $this->general->profile($id);
        $data['user_id'] = $id; 
        $this->load->view('english/public_profile',$data);
   }
   public function add_vote(){
     $login_user_id=$this->input->post('login_user_id');
     $question_id_for_vote=$this->input->post('question_id_for_vote');
     $data1= array(
      'q_id' => $question_id_for_vote,
      'user_id' => $login_user_id
     );
     $insert=$this->general->insert_vote($data1);  

    
      $this->db->select('user_id');
      $this->db->from('tbl_questions');
      $this->db->where('id',$question_id_for_vote);
      $q_user = $this->db->get()->row()->user_id; 

       $login_user =  $this->general->get_user($login_user_id);
       $qu_user =  $this->general->get_user($q_user);

       $msg = $login_user." "."Voted On ".$qu_user." "." Question";
       $data2 = array(
          'user_id' => $login_user_id,
          'msg' => $msg,
          'q_id' => $question_id_for_vote,
          'to_user_id'=>$q_user,
          'status' =>1
      );
      $this->db->insert('tbl_notifications',$data2);

     $query = $this->db->query("SELECT * FROM tbl_votes where q_id='".$question_id_for_vote."'");
     $cnt =  $query->num_rows(); 
     echo json_encode(array('result'=>'success','cnt' => $cnt));
   }
   public function remove_vote(){
     $login_user_id=$this->input->post('login_user_id');
     $question_id_for_vote=$this->input->post('question_id_for_vote');
     
     $this -> db -> where('tbl_votes.q_id', $question_id_for_vote);
     $this -> db -> where('tbl_votes.user_id', $login_user_id);
     $this -> db -> delete('tbl_votes');

     $query = $this->db->query("SELECT * FROM tbl_votes where q_id='".$question_id_for_vote."'");
     $cnt =  $query->num_rows(); 
     echo json_encode(array('result'=>'success','cnt' => $cnt));
   }
   public function add_vote_post(){
     $login_user_id=$this->input->post('login_user_id');
     $post_id_for_vote=$this->input->post('question_id_for_vote');
     $data1= array(
      'post_id' => $post_id_for_vote,
      'user_id' => $login_user_id
     );
     $insert=$this->general->insert_vote_post($data1);  

      $this->db->select('user_id');
      $this->db->from('tbl_questions');
      $this->db->where('id',$post_id_for_vote);
      $po_user_id = $this->db->get()->row()->user_id; 

       $login_user =  $this->general->get_user($login_user_id);
       $po_user =  $this->general->get_user($po_user_id);

       $msg = $login_user." "."Voted On ".$po_user." "." Post";
       $data2 = array(
          'user_id' => $login_user_id,
          'msg' => $msg,
          'p_id' => $post_id_for_vote,
          'to_user_id'=>$po_user_id,
          'status' =>1
      );
      $this->db->insert('tbl_notifications',$data2);

     $query = $this->db->query("SELECT * FROM tbl_votes_post where post_id='".$post_id_for_vote."'");
     $cnt =  $query->num_rows(); 
     echo json_encode(array('result'=>'success','cnt' => $cnt));
   }
   public function remove_vote_post(){
     $login_user_id=$this->input->post('login_user_id');
     $post_id_for_vote=$this->input->post('question_id_for_vote');
     
     $this -> db -> where('tbl_votes_post.post_id', $post_id_for_vote);
     $this -> db -> where('tbl_votes_post.user_id', $login_user_id);
     $this -> db -> delete('tbl_votes_post');

     $query = $this->db->query("SELECT * FROM tbl_votes_post where post_id='".$post_id_for_vote."'");
     $cnt =  $query->num_rows(); 
     echo json_encode(array('result'=>'success','cnt' => $cnt));
   }
   public function remove_notification(){
    $login_user_id=$this->input->post('login_user_id');
    $data = array(
      'status' => 0
    );
     $this->db->where('to_user_id', $login_user_id);
     $this->db->update('tbl_notifications', $data);
     echo json_encode(array('result'=>'success'));
   }
   public function add_answer(){
     $login_user_id=$this->input->post('login_user_id');
     $comment=$this->input->post('comment');
     $q_id = $this->input->post('q_id');
     $p_id = $this->input->post('p_id');
     $data1= array(
      'parent_comment_id' => $p_id,
      'comment' => $comment,
      'question_id' => $q_id,
      'user_id' => $login_user_id
     );

      $this->db->select('user_id');
      $this->db->from('tbl_questions');
      $this->db->where('id',$q_id);
      $q_user = $this->db->get()->row()->user_id;


     $get_user = $this->general->get_user($login_user_id); 
     $o_user =  $this->general->get_user($q_user); 
     $msg = $get_user." "."Commented On ".$o_user." "." Question";
     $data2 = array(
        'user_id' => $login_user_id,
        'msg' => $msg,
        'q_id' => $q_id,
        'to_user_id'=>$q_user,
        'status' =>1
    );
     $insert=$this->general->save_answer($data1,$data2);  



     echo json_encode(array('result'=>'success'));
   }
   public function update_answer(){
     $login_user_id=$this->input->post('login_user_id');
     $comment=$this->input->post('comment');
     $q_id = $this->input->post('q_id');
    
     $m_id = $this->input->post('m_id');
     $data1= array(
      
      'comment' => $comment,
      'question_id' => $q_id,
      'user_id' => $login_user_id
     );
    $this->db->where('id', $m_id);
    $this->db->update('tbl_answer', $data1);


     echo json_encode(array('result'=>'success'));
   }
   public function remove_comment(){
    $id=$this->input->post('id');
    $this->general->comment_delete($id);
   }
   public function remove_post_comment(){
    $id=$this->input->post('id');
    $this->general->post_comment_delete($id);
   }
   public function add_post_answer(){
     $login_user_id=$this->input->post('login_user_id');
     $comment=$this->input->post('comment');
     $post_id = $this->input->post('post_id');
     $p_id = $this->input->post('p_id');
     $data1= array(
      'parent_comment_id' => $p_id,
      'comment' => $comment,
      'post_id' => $post_id,
      'user_id' => $login_user_id
     );
      $this->db->select('user_id');
      $this->db->from('tbl_post');
      $this->db->where('id',$post_id);
      $p_user = $this->db->get()->row()->user_id;


     $get_user = $this->general->get_user($login_user_id); 
     $o_user =  $this->general->get_user($p_user); 
     $msg = $get_user." "."Commented On ".$o_user." "." Post";
       $data2 = array(
          'user_id' => $login_user_id,
          'msg' => $msg,
          'p_id' => $post_id,
          'to_user_id'=>$p_user,
          'status' =>1
      );
     $insert=$this->general->save_post_answer($data1,$data2);  


     echo json_encode(array('result'=>'success'));
   }
   public function update_post_answer(){
     $login_user_id=$this->input->post('login_user_id');
     $comment=$this->input->post('comment');
     $post_id = $this->input->post('post_id');
    
     $m_id = $this->input->post('m_id');
     $data1= array(
      
      'comment' => $comment,
      'post_id' => $post_id,
      'user_id' => $login_user_id
     );
    $this->db->where('id', $m_id);
    $this->db->update('tbl_post_answer', $data1);


     echo json_encode(array('result'=>'success'));
   }
   public function add_answer_mal(){
     $login_user_id=$this->input->post('login_user_id');
     $comment=$this->input->post('comment');
     $q_id = $this->input->post('q_id');
     $p_id = $this->input->post('p_id');
     $data1= array(
      'parent_comment_id' => $p_id,
      'comment' => $comment,
      'question_id' => $q_id,
      'user_id' => $login_user_id,
      'lan' =>1
     );
     $this->db->select('user_id');
      $this->db->from('tbl_questions');
      $this->db->where('id',$q_id);
      $q_user = $this->db->get()->row()->user_id;


     $get_user = $this->general->get_user($login_user_id); 
     $o_user =  $this->general->get_user($q_user); 
     $msg = $get_user." "."Commented On ".$o_user." "." Question";
       $data2 = array(
          'user_id' => $login_user_id,
          'msg' => $msg,
          'q_id' => $q_id,
          'to_user_id' => $q_user,
          'status' =>1
      );
     $insert=$this->general->save_answer($data1,$data2);  


     echo json_encode(array('result'=>'success'));
   }
   public function add_post_answer_mal(){
    $login_user_id=$this->input->post('login_user_id');
     $comment=$this->input->post('comment');
     $post_id = $this->input->post('post_id');
     $p_id = $this->input->post('p_id');
     $data1= array(
      'parent_comment_id' => $p_id,
      'comment' => $comment,
      'post_id' => $post_id,
      'user_id' => $login_user_id,
      'lan' => 1
     );
      $this->db->select('user_id');
      $this->db->from('tbl_post');
      $this->db->where('id',$post_id);
      $p_user = $this->db->get()->row()->user_id;


     $get_user = $this->general->get_user($login_user_id); 
     $o_user =  $this->general->get_user($p_user); 
     $msg = $get_user." "."Commented On ".$o_user." "." Post";
       $data2 = array(
          'user_id' => $login_user_id,
          'msg' => $msg,
          'p_id' => $post_id,
          'to_user_id' => $p_user,
          'status' =>1
      );
     $insert=$this->general->save_post_answer($data1,$data2);  


     echo json_encode(array('result'=>'success'));
   }
    public function user_logout() {
        $this->session->unset_userdata('user_logged_in');
        redirect(base_url());
    }

}

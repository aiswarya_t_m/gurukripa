<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct()
    {
        parent::__construct();

        $this->load->model('General_model','general');
        $this->load->model('Admin_model','admin');

    }
	public function index()
	{
      $data['religion_list'] = $this->admin->religion_list();
      $data['states'] = $this->general->get_states();
      $data['featured_list'] = $this->general->get_featured();
      $this->load->view('website/home_page',$data);
	}
  public function aboutus(){
    $this->load->view('website/aboutus');
  }
  public function search_profile(){
    $this->load->view('website/search_profile');
  }
	public function login(){
		     $this->form_validation->set_rules('username', 'Username', 'trim|required');
         $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            if (isset($this->session->userdata['admin_logged_in'])) {
                redirect(base_url().'adminsections');
            }
            elseif (isset($this->session->userdata['user_logged_in'])){
                redirect(base_url().'userhome');
            }
            else {
             $this->load->view('website/login');
            }
        } else {
        	$password = $this->input->post('password');
            $data = array(
                'username' => $this->input->post('username'),
                'password' => md5($password)
            );
            $result = $this->general->login($data);
            if ($result == TRUE) {

                $username = $this->input->post('username');
                $result = $this->general->read_user_information($username);
                if ($result != false) {
                    if ($result[0]->role == 'Admin') {
                        $this->session->set_userdata('admin_logged_in',array('username' => $result[0]->username,'user_id' => $result[0]->id));
                        redirect(base_url().'adminsections');
                    } elseif (($result[0]->role == 'User')) {
                        $this->session->set_userdata('user_logged_in',array('username' => $result[0]->username,'user_id' => $result[0]->id));
                        redirect(base_url().'userhome');
                    }
                    else{

                        $this->load->view('website/login',array('error_message' => 'You are not authorized user..!'));
                    }
                }
            }

            else {

                $this->load->view('website/login',array('error_message' => 'Invalid Username or Password'));



            }
        }
	}
	public function quick_register(){

    $data['religion_list'] = $this->admin->religion_list();
    $data['states'] = $this->general->get_states();

		if(isset($_POST['sbt'])){

       $this->form_validation->set_rules('prof_for', 'prof_for', 'required');
       $this->form_validation->set_rules('gender', 'gender', 'required');
			 $this->form_validation->set_rules('full_name', 'full_name', 'required');
       $this->form_validation->set_rules('dob', 'dob', 'required');
       $this->form_validation->set_rules('religion_name', 'religion', 'required');
       $this->form_validation->set_rules('caste', 'caste', 'required');
       $this->form_validation->set_rules('country', 'country', 'required');
       $this->form_validation->set_rules('s_name', 'state', 'required');
       $this->form_validation->set_rules('district', 'district', 'required');
       
			 $this->form_validation->set_rules('email_id', 'Email', 'required');

			 $this->form_validation->set_rules('mob_no', 'Mobile Number', 'required');

			 $this->form_validation->set_rules('password', 'Password', 'required');
           

			 if ($this->form_validation->run() == FALSE)
                {
                        $data['error_msg'] = "All Fields Must Be Required And Valid";

                        $this->load->view('website/home_page',$data);
                }
                else
                {
                	$password = $this->input->post('password');

                     $data=array(
                    'full_name'=>$this->input->post('full_name'),
                    'profile_for'=>$this->input->post('prof_for'),
                    'gender'=>$this->input->post('gender'),
                    'dob'=>date("Y-m-d",strtotime($this->input->post('dob'))),
                    'religion'=>$this->input->post('religion_name'),
                    'caste'=>$this->input->post('caste'),
                    'country'=>$this->input->post('country'),
                    'state'=>$this->input->post('s_name'),
                    'district'=>$this->input->post('district'),
                    'mobile_no'=>$this->input->post('mob_no'),
                    'email_id'=>$this->input->post('email_id'),
                    'username'=>$this->input->post('email_id'),
                    'password'=>md5($this->input->post('password')),
                    'role'=>'User',
               			 );

	                if($this->general->form_register_insert($data))
	                {
              
	                    $data['message'] = 'You Are Registered Successfully';
                		$this->load->view('website/home_page',$data);

	                }
	                else
	                {

	                   $data['message'] = 'Registration Failed';
               		   $this->load->view('website/home_page',$data);
	                }
                }
		}

		else{
		$this->load->view('english/home_page',$data);
		}

	}
    public function user_profile_public($id){
        $data['results'] =  $this->general->profile($id);
        $data['user_id'] = $id; 
        $this->load->view('english/public_profile',$data);  
   }
   public function view_question($q_id){
      $this->db->select('view_count');
      $this->db->from('tbl_views');
      $this->db->where('question_id',$q_id);
      $view_count = $this->db->get()->row();
          if(empty($view_count)){
              $count = 1;
              $data = array(
                    'question_id' => $q_id,
                    'view_count'  => $count

                );
                $this->db->insert('tbl_views',$data);
             }
             else{
                $count = $view_count->view_count + 1;
                 $data = array(
                    'question_id' => $q_id,
                    'view_count'  => $count

                );
                $this->db->where('question_id', $q_id);
                $this->db->update('tbl_views', $data);
             }
    

    $data['results']=  $this->general->categorylist();
    $data['no_of_users'] = $this->general->get_no_users();
    $data['question_details'] =  $this->general->details_of_question($q_id);
    $this->load->view('english/view_question',$data);
   }
   public function view_posts($post_id){
      $this->db->select('view_count');
      $this->db->from('tbl_post_view');
      $this->db->where('post_id',$post_id);
      $view_count = $this->db->get()->row();
          if(empty($view_count)){
              $count = 1;
              $data = array(
                    'post_id' => $post_id,
                    'view_count'  => $count

                );
                $this->db->insert('tbl_post_view',$data);
             }
             else{
                $count = $view_count->view_count + 1;
                 $data = array(
                    'post_id' => $post_id,
                    'view_count'  => $count

                );
                $this->db->where('post_id', $post_id);
                $this->db->update('tbl_post_view', $data);
             }
    

    $data['results']=  $this->general->categorylist();
    $data['no_of_users'] = $this->general->get_no_users();
    $data['post_details'] =  $this->general->details_of_post($post_id);
    $this->load->view('english/view_post',$data);
   }
   public function related($id){
        $data['results']=  $this->general->categorylist();
        $data['no_of_users'] = $this->general->get_no_users();
        $data['c_id'] = $id;
        $this->load->view('english/related_posts',$data);
   }

public function get_district(){



    $s_id = $_POST['states_val'];
    $this->db->select('*');
    $this->db->from('tbl_district');
    $this->db->where('state_id',$s_id);
    $data['res'] = $this->db->get()->result_array();

   
    $drpdwn =  $this->load->view('website/disdropdown',$data,true);
        echo json_encode(array('result'=>'success','drpdwn'=>$drpdwn));


}


public function get_caste(){



    $rel_val = $_POST['rel_val'];
    $this->db->select('*');
    $this->db->from('tbl_caste');
    $this->db->where('religion_id',$rel_val);
    $data['res'] = $this->db->get()->result_array();

   
    $drpdwn =  $this->load->view('website/castedropdown',$data,true);
        echo json_encode(array('result'=>'success','drpdwn'=>$drpdwn));


}

     public function packages(){

      $this->load->view('website/packages');
     }


    public function contactus(){

      $this->load->view('website/contactus');
     }



}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminsections extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        // if (!(isset($this->session->userdata['admin_logged_in']))) {
        //     redirect(base_url());
        // }
        $this->load->model('Admin_model','admin');
    }

    public function index(){
        // $data['no_of_users'] = $this->general_model->get_no_users();
        // $data['no_of_categories'] = $this->general_model->get_no_catgry();
        // $data['no_of_questions'] = $this->general_model->get_no_questions();
        // $data['no_of_posts'] = $this->general_model->get_no_posts();
        $this->load->view('admin_panel/admin_home');

    }
    public function admin_logout() {
        $this->session->unset_userdata('admin_logged_in');
        redirect(base_url());
    }
    public function religion(){
        $data['results']=  $this->admin->religion_list();
        $this->load->view('admin_panel/religion_list',$data);
    }
    public function add_religion(){

    

        if (isset($_POST['sbt']))
        {
             $this->form_validation->set_rules('rel_name', 'Religion Name', 'required');
             

             if ($this->form_validation->run() == FALSE)
                {
                        $this->load->view('admin_panel/add_religion');
                }
                else
                {
            $data1 = array(
                'name' =>$this->input->post('rel_name'),
              
            );
            if($this->admin->form_religion_insert($data1))
            {
              
                $data['message'] = 'Religion Saved Successfully';
                $this->load->view('admin_panel/add_religion',$data);

            }
            else{
        
                $data['message'] ='Data Insertion Failed';
                $this->load->view('admin_panel/add_religion',$data);
            }
          
            }
        }
        else{
             $this->load->view('admin_panel/add_religion');
        }
      
    }
    public function edit_religion($r_id){
       if (isset($_POST['sbt']))
        {
             $this->form_validation->set_rules('rel_name', 'Religion Name', 'required');
             

             if ($this->form_validation->run() == FALSE)
                {
                        $data['reslt'] = $this->admin->get_religion($r_id);
                        $this->load->view('admin_panel/edit_religion',$data);
                }
                else
                {
                    $data1 = array(
                        'name' =>$this->input->post('rel_name'),
                     
                    );
                    if($this->admin->form_religion_edit($data1,$r_id))
                    {
                        $data['reslt'] = $this->admin->get_religion($r_id);
                        $data['message'] = 'Data Updated Successfully';
                        $this->load->view('admin_panel/edit_religion',$data);

                    }
                    else{
                        $data['reslt'] = $this->admin->get_religion($r_id);
                        $data['message'] ='Data Updation Failed';
                        $this->load->view('admin_panel/edit_religion',$data);
                    }
                }
            
        }
        else{
             $data['reslt'] = $this->admin->get_religion($r_id);
             $this->load->view('admin_panel/edit_religion',$data);
        }
    }
    public function remove_religion(){
        $id=$this->input->post('id');
        $this->admin->religion_delete($id);
    }
    public function users(){
       $data['results'] = $this->admin->get_users();
       $this->load->view('admin_panel/users',$data); 
    }

    public function view_user($id){

       $data['reslt'] = $this->admin->get_user_details($id);
       $this->load->view('admin_panel/view_user_details',$data); 


    }

    public function paid_users(){
       $data['results'] = $this->admin->get_paid_users();
       $this->load->view('admin_panel/paid_users',$data); 
    }
  


 public function add_caste(){

  $data['religions'] = $this->admin->religion_list();
        if (isset($_POST['sbt']))
        {
             $this->form_validation->set_rules('caste_name', 'Caste Name', 'required');
             $this->form_validation->set_rules('religion_id', 'Religion Name', 'required');
             

             if ($this->form_validation->run() == FALSE)
                {
                        $this->load->view('admin_panel/add_caste');
                }
                else
                {
            $data1 = array(
                'name' =>$this->input->post('caste_name'),
                'religion_id' =>$this->input->post('religion_id'),
              
            );
            if($this->admin->form_caste_insert($data1))
            {
              
                $data['message'] = 'Caste Saved Successfully';
                $this->load->view('admin_panel/add_caste',$data);

            }
            else{
        
                $data['message'] ='Data Insertion Failed';
                $this->load->view('admin_panel/add_caste',$data);
            }
          
            }
        }
        else{
             $this->load->view('admin_panel/add_caste',$data);
        }
      
    } 

    public function edit_caste($c_id){

      

      $data['religions'] = $this->admin->religion_list();
       if (isset($_POST['sbt']))
        {
            $this->form_validation->set_rules('caste_name', 'Caste Name', 'required');
             $this->form_validation->set_rules('religion_id', 'Religion Name', 'required');
             
             

             if ($this->form_validation->run() == FALSE)
                {
                        $data['reslt'] = $this->admin->get_caste($c_id);
                        $this->load->view('admin_panel/edit_caste',$data);
                }
                else
                {
                    $data1 = array(
                        'name' =>$this->input->post('caste_name'),
                        'religion_id' =>$this->input->post('religion_id'),
                     
                    );
                    if($this->admin->form_caste_edit($data1,$c_id))
                    {
                        $data['reslt'] = $this->admin->get_caste($c_id);
                        $data['message'] = 'Data Updated Successfully';
                        $this->load->view('admin_panel/edit_caste',$data);

                    }
                    else{
                        $data['reslt'] = $this->admin->get_caste($c_id);
                        $data['message'] ='Data Updation Failed';
                        $this->load->view('admin_panel/edit_caste',$data);
                    }
                }
            
        }
        else{
             $data['reslt'] = $this->admin->get_caste($c_id);
      
             $this->load->view('admin_panel/edit_caste',$data);
        }
    }

     public function remove_caste(){
        $id=$this->input->post('id');
        $this->admin->caste_delete($id);
    }

    public function caste(){

        $data['results']=  $this->admin->caste_list();
        $this->load->view('admin_panel/caste_list',$data);

    }


    public function add_package(){

 
        if (isset($_POST['sbt']))
        {
             $this->form_validation->set_rules('pack_des', 'Description', 'required');
             $this->form_validation->set_rules('price', 'Price', 'required');
             

             if ($this->form_validation->run() == FALSE)
                {
                        $this->load->view('admin_panel/add_package');
                }
                else
                {
            $data1 = array(
                'name' =>$this->input->post('pack_name'),
                'description' =>$this->input->post('pack_des'),
                'price' =>$this->input->post('price'),
              
            );
            if($this->admin->form_package_insert($data1))
            {
              
                $data['message'] = 'Packages Saved Successfully';
                $this->load->view('admin_panel/add_package',$data);

            }
            else{
        
                $data['message'] ='Data Insertion Failed';
                $this->load->view('admin_panel/add_package',$data);
            }
          
            }
        }
        else{
             $this->load->view('admin_panel/add_package');
        }
      
    } 

    public function edit_package($c_id){

      

     
       if (isset($_POST['sbt']))
        {
           $this->form_validation->set_rules('pack_des', 'Description', 'required');
             $this->form_validation->set_rules('price', 'Price', 'required');
             
             if ($this->form_validation->run() == FALSE)
                {
                        $data['reslt'] = $this->admin->get_package($c_id);
                        $this->load->view('admin_panel/edit_package',$data);
                }
                else
                {
                    $data1 = array(
                         'name' =>$this->input->post('pack_name'),
                        'description' =>$this->input->post('pack_des'),
                        'price' =>$this->input->post('price'),
              
                    );
                    if($this->admin->form_package_edit($data1,$c_id))
                    {
                        $data['reslt'] = $this->admin->get_package($c_id);
                        $data['message'] = 'Data Updated Successfully';
                        $this->load->view('admin_panel/edit_package',$data);

                    }
                    else{
                        $data['reslt'] = $this->admin->get_package($c_id);
                        $data['message'] ='Data Updation Failed';
                        $this->load->view('admin_panel/edit_package',$data);
                    }
                }
            
        }
        else{
             $data['reslt'] = $this->admin->get_package($c_id);
      
             $this->load->view('admin_panel/edit_package',$data);
        }
    }

     public function remove_package(){
        $id=$this->input->post('id');
        $this->admin->package_delete($id);
    }

    public function packages(){

        $data['results']=  $this->admin->package_list();
        $this->load->view('admin_panel/package_list',$data);

    }

}
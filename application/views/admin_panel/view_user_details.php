<?php  $this->load->view('admin_panel/admin_includes'); ?>
<?php $this->load->view('admin_panel/admin_sidebar');?>
<!-- Contents -->
<main class="l-main">
  <div class="content-wrapper content-wrapper--with-bg">
    <h3 class="page-title"></h3>
    <div class="page-content">
    	<div class="container">
    		

             <div class="row">
             	
                     <div class="col-md-4">

                     	<?php

                     	if($reslt->profile_img!=''){ ?>
                     	
                       <img src="<?php echo base_url('assets/profiles').'/'.$reslt->profile_img;?>" class="right_side_listing_image">

                       <?php
                   }

                   else{ ?>

                   	 <img src="<?php echo base_url('assets/images/download.jpg');?>" class="right_side_listing_image">

                   	<?php } ?>

                     </div>

                     <div class="col-md-8">
                     	
                          <span class="p_id"><h3>Profile ID : </h3></span>
                          <div class="row">
                          	  
                          <div class="d1 col-md-4">Name :</div><div class="s1 col-md-6"><?php echo $reslt->full_name;?></div>
                          <div class="d1 col-md-4">Gender : </div><div class="s1 col-md-6"><?php echo $reslt->gender;?></div>
                          <div class="d1 col-md-4">Date Of Birth : </div><div class="s1 col-md-6"><?php echo $reslt->dob;?></div>
                            <div class="d1 col-md-4">Religion :</div><div class="s1 col-md-6"><?php echo $reslt->religion;?></div>
                           <div class="d1 col-md-4">Caste : </div><div class="s1 col-md-6"><?php echo $reslt->caste;?></div>

                           <div class="d1 col-md-4">Address :</div><div class="s1 col-md-6"><?php echo $reslt->address;?></div>

                          <div class="d1 col-md-4">Country : </div><div class="s1 col-md-6"><?php echo $reslt->country;?></div>
                           <div class="d1 col-md-4">State :</div><div class="s1 col-md-6"><?php echo $reslt->state;?></div>
                           <div class="d1 col-md-4">District :</div><div class="s1 col-md-6"><?php echo $reslt->district;?></div>

                          <div class="d1 col-md-4">Email ID : </div><div class="s1 col-md-6"><?php echo $reslt->email_id;?></div>
                           <div class="d1 col-md-4">Mobile NO : </div><div class="s1 col-md-6"><?php echo $reslt->mobile_no;?></div>
                          <div class="d1 col-md-4">Profile For : </div><div class="s1 col-md-6"><?php echo $reslt->profile_for;?></div>


                          </div>

                </div>

             </div>

             <div class="row">
             	
                <div class="box_dtl">
                	
                 
                     <div class="col-md-12">
                     	
                       <h3>Brief About </h3>
                          <div class="about_me"><?php echo $reslt->about_me;?></div>

                     </div>

                      <div class="col-md-12">
                     	
                       <h3>Family Details</h3>
                         
                         <div class="row">
                          <div class="d1 col-md-4">Father's Name :</div><div class="s1 col-md-6"><?php echo $reslt->father_name;?></div>
                           <div class="d1 col-md-4">Mother's Name : </div><div class="s1 col-md-6"><?php echo $reslt->mother_name;?></div>
                           <div class="d1 col-md-4">Father's Occupation : </div><div class="s1 col-md-6"><?php echo $reslt->father_occupation;?></div>
                           <div class="d1 col-md-4">Mother's Occupation : </div><div class="s1 col-md-6"><?php echo $reslt->mother_occupation;?></div>
                           <div class="d1 col-md-4">No of Married Brothers : </div><div class="s1 col-md-6"><?php echo $reslt->married_bro;?></div>
                           <div class="d1 col-md-4">No of Unmarried Brothers : </div><div class="s1 col-md-6"><?php echo $reslt->unmarried_bro;?></div>
                           <div class="d1 col-md-4">No of Married Sisters : </div><div class="s1 col-md-6"><?php echo $reslt->married_sis;?></div>
                           <div class="d1 col-md-4">No of Unmarried Sisters : </div><div class="s1 col-md-6"><?php echo $reslt->unmarried_sis;?></div>
                      </div>

                     </div>

                      <div class="col-md-12">
                     	
                       <h3>Education Details</h3>
                        <div class="row"> 

                          <div class="d1 col-md-4">Qualification : </div><div class="s1 col-md-6"><?php echo $reslt->qualification;?></div>
                         <div class="d1 col-md-4">Designation :</div><div class="s1 col-md-6"><?php echo $reslt->designation;?></div>
                          <div class="d1 col-md-4">Monthly Income : </div><div class="s1 col-md-6"><?php echo $reslt->monthly_income;?></div>
                         </div>

                     </div>
                     

                     <div class="col-md-12">
                     	
                       <h3>Life Style</h3>
                         <div class="row">

                          <div class="d1 col-md-4">Current Location :  </div><div class="s1 col-md-6"><?php echo $reslt->current_place;?></div>
                          <div class="d1 col-md-4">Marital Status :  </div><div class="s1 col-md-6"><?php echo $reslt->marital_status;?></div>
                          <div class="d1 col-md-4">Height :  </div><div class="s1 col-md-6"><?php echo $reslt->height;?></div>
                         <div class="d1 col-md-4">Weight :  </div><div class="s1 col-md-6"><?php echo $reslt->weight;?></div>
                         <div class="d1 col-md-4">Complexion : </div><div class="s1 col-md-6"><?php echo $reslt->complexion;?></div>
                          <div class="d1 col-md-4">Physically Challenged: </div><div class="s1 col-md-6"><?php echo $reslt->health_problem;?></div>
                          <div class="d1 col-md-4">Eating Habits : </div><div class="s1 col-md-6"><?php echo $reslt->eating_habit;?></div>
                          <div class="d1 col-md-4">Family Status : </div><div class="s1 col-md-6"><?php echo $reslt->family_status;?></div>
                          <div class="d1 col-md-4">Family Values : </div><div class="s1 col-md-6"><?php echo $reslt->family_values;?></div>
                         </div>

                     </div>

                     <div class="col-md-12">
                     	
                       <h3>Requirements For Partner</h3>
                         
                       <span class="s1"><?php echo $reslt->requirements;?></span>
                         

                     </div>

                      <div class="col-md-12">
                     	
                       <h3>Horoscope</h3>
                         <div class="row">
                         <div class="d1 col-md-4">Birth Star & Shishtadasa :</div><div class="s1 col-md-6"><?php echo $reslt->star_dasa;?></span></div>
                         <div class="d1 col-md-4">Type Of Horoscope : </div><div class="s1 col-md-6"><?php echo $reslt->type_of_horoscope;?></span></div>
                         </div>

                     </div>



                </div>

             </div>


    	</div>
    </div>
  </div>
</main>
<!-- Contents Ends -->
<?php $this->load->view('admin_panel/admin_footer');?>

<header class="l-header">
    <div class="l-header__inner clearfix">
      <div class="c-header-icon js-hamburger">
        <div class="hamburger-toggle"><span class="bar-top"></span><span class="bar-mid"></span><span class="bar-bot"></span></div>
      </div>
      <!-- <div class="c-header-icon has-dropdown"> --><!-- <span class="c-badge c-badge--header-icon animated shake">87</span><i class="fa fa-bell"></i>
        <div class="c-dropdown c-dropdown--notifications">
          <div class="c-dropdown__header"></div>
          <div class="c-dropdown__content"></div>
        </div> -->
     <!--  </div> -->
      <div class="c-search">
        <h6 class="login_date">Date : <?php echo  date('d-M-Y');?></h6>
      </div>
      <div class="header-icons-group">
        <!-- <div class="c-header-icon basket"><span class="c-badge c-badge--header-icon animated shake">12</span><i class="fa fa-shopping-basket"></i></div> -->
        <a href="<?php echo base_url() ?>Adminsections/admin_logout"><div class="c-header-icon logout"><i class="fa fa-power-off"></i></div></a>
      </div>
    </div>
  </header>
  <div class="l-sidebar">
    <div class="logo">
      <div class="logo__txt">GuruKripa</div>
    </div>
    <div class="l-sidebar__content">
      <nav class="c-menu js-menu">
        <ul class="u-list">
          <li class="c-menu__item" data-toggle="tooltip" title="Dashboard">
            <a class="a_href_in_admin_panel"  href="<?php echo base_url() ?>Adminsections"><div class="c-menu__item__inner"><i class="fa fa-dashboard"></i> 
              <div class="c-menu-item__title"><span>Dashboard</span></div>
            </div></a>
          </li>
          <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Religion">
            <a class="a_href_in_admin_panel" href="<?php echo base_url() ?>Adminsections/religion"><div class="c-menu__item__inner"><i class="fa fa-star"></i>
              <div class="c-menu-item__title"><span>Add Religion</span></div>
            </div></a>
          </li>

          <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Caste">
            <a class="a_href_in_admin_panel" href="<?php echo base_url() ?>Adminsections/caste"><div class="c-menu__item__inner"><i class="fa fa-list"></i>
              <div class="c-menu-item__title"><span>Add Caste</span></div>
            </div></a>
          </li>

           

           <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Packages">
            <a class="a_href_in_admin_panel" href="<?php echo base_url() ?>Adminsections/packages"><div class="c-menu__item__inner"><i class="fa fa-gift"></i>
              <div class="c-menu-item__title"><span>Add Packages</span></div>
            </div></a>
          </li>

          <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Users">
            <a class="a_href_in_admin_panel"  href="<?php echo base_url() ?>Adminsections/users"><div class="c-menu__item__inner"><i class="fa fa-users"></i>
              <div class="c-menu-item__title"><span>Users</span></div>
            </div></a>
          </li>

          <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Users">
            <a class="a_href_in_admin_panel"  href="<?php echo base_url() ?>Adminsections/paid_users"><div class="c-menu__item__inner"><i class="fa fa-money"></i>
              <div class="c-menu-item__title"><span>Paid Users</span></div>
            </div></a>
          </li>
         
        </ul>
      </nav>
    </div>
  </div>


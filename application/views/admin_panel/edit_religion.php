<?php  $this->load->view('admin_panel/admin_includes'); ?>
<?php $this->load->view('admin_panel/admin_sidebar');?>
<!-- Contents -->
<main class="l-main">
  <div class="content-wrapper content-wrapper--with-bg">
    <h3 class="page-title">Edit Religion</h3>
    <div class="page-content">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <span class="btn btn-primary btn_cmn"><a href="<?php echo base_url();?>Adminsections/religion" class="a_href_in_admin_panel"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;List Category</a></span>
                </div>
                <div class="col-sm-12">

                <?php 
                  if(isset($message)){
                    ?>
                    <div class="alert alert-success">
                        <strong><?php echo $message?></strong>
                    </div>
                    <?php
                  }
                ?>
                <?php if((form_error('name')) != false) { ?>
                 <div class="alert alert-danger">
                  <strong><?php echo  form_error('name') ?></strong> 
                </div>
             <?php } ?>
            
                 <?php  echo form_open('Adminsections/edit_religion/'.$reslt->id, ['id' => 'frmUsers']); ?>
                  
                   
                    <label>Religion</label>
                    <input type="text" class="form-element-l" autocomplete="off" name="rel_name" id="rel_name" placeholder="Religion" value="<?php echo $reslt->name ;?>">
                 
                  
                   <button type="submit" name="sbt" class="btn btn-primary">Update</button>
                 <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</main>
<!-- Contents Ends -->
<?php $this->load->view('admin_panel/admin_footer');?>

<?php  $this->load->view('admin_panel/admin_includes'); ?>
<?php $this->load->view('admin_panel/admin_sidebar');?>
<!-- Contents -->
 <?php 
 if ((isset($this->session->userdata['admin_logged_in']))){
  $sesn = $this->session->userdata['admin_logged_in'];   
  $user_id = $sesn['user_id'];
  $this->load->model('general_model','general');
     $ci = &get_instance();
    // $notifications = $ci->general->get_all_notifications($user_id);
 }
 
 ?>
<main class="l-main">
  <div class="content-wrapper content-wrapper--with-bg">
    <h3 class="page-title">Dashboard</h3>
   <!--  <div class="page-content">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-4 text-center">
    				<div class="row">
                        <div class="col-sm-6">
                            <a class="a_href_in_box_no" href="<?php echo base_url('Adminsections/category');?>">
                                <div class="box_no">
                                 <h4><strong><?php echo $no_of_categories;?></strong></h4>
                                 <h6>Categories</h6>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a class="a_href_in_box_no"  href="<?php echo base_url('Adminsections/users');?>">
                                <div class="box_no">
                                    <h4><strong><?php echo $no_of_users;?></strong></h4>
                                    <h6>Users</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row pt-4">
                        <div class="col-sm-6">
                            <a class="a_href_in_box_no"  href="<?php echo base_url('Adminsections/questions');?>">
                                <div class="box_no">
                                    <h4><strong></strong><?php echo $no_of_questions;?></strong></h4>
                                    <h6>Questions</h6>
                                </div>
                             </a>
                        </div>
                        <div class="col-sm-6">
                            <a class="a_href_in_box_no"  href="<?php echo base_url('Adminsections/posts');?>">
                                <div class="box_no">
                                    <h4><strong><?php echo $no_of_posts;?></strong></h4>
                                    <h6>Posts</h6>
                                </div>
                            </a>
                        </div>
                    </div>
    			</div>
    			<div class="col-sm-8">
    				<?php 
        if(!empty($notifications)){
		      ?>
		      <div class="card card_m card_cmn">
		      <h6 class="cmn_hdng">Notifications</h6>
		        <?php 
		          foreach ($notifications as $key => $value) {
		            $qid = $value['q_id'];
		            $pid = $value['p_id'];
		             if($pid == 0){
		              $q_details = $ci->general->get_q_details($qid);
		              $link =  base_url('Adminsections/questions');
		              ?>
		              <p class="cmn_p"><?php echo $value['msg']." <a href='".$link."' class='cmn_a'>".$q_details ."</a> at ".$value['created_at'];?></p>
		              <?php
		             }
		             else{
		              $p_details = $ci->general->get_p_details($pid);
		              $link =  base_url('Adminsections/posts');
		              ?>
		              <p class="cmn_p"><?php echo $value['msg']." <a href='".$link."' class='cmn_a'>".$p_details ."</a> at ".$value['created_at'];?></p>
		              <?php
		             }
		           ?>
		           

		           <?php
		          }
		        ?>
		      </div>
		  
		      <?php 
		       }
		      ?>
    			</div>
    		</div>
    	</div>
    </div>
  </div> -->
</main>
<!-- Contents Ends -->
<?php $this->load->view('admin_panel/admin_footer');?>
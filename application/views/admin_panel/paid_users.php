<?php  $this->load->view('admin_panel/admin_includes'); ?>
<?php $this->load->view('admin_panel/admin_sidebar');?>
<!-- Contents -->
<main class="l-main">
  <div class="content-wrapper content-wrapper--with-bg">
    <h3 class="page-title">User List</h3>
    <div class="page-content">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
                 <?php
                   if(isset($message)){
                    ?>
                    <div class="alert alert-success">
                        <strong><?php echo $message?></strong>
                    </div>
                    <?php 
                   }
                  ?>
    				<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Amount</th>
                           
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;foreach($results as $rec):
                            ?>
                            <tr id="<?php echo $rec['id']; ?>">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $rec['full_name']; ?></td>
                                <td><?php echo $rec['email_id']; ?></td>
                                <td><?php echo $rec['mobile_no']; ?></td>
                                <td><?php echo $rec['price']; ?></td>
                                  <td>
                                <a href="<?php echo base_url('Adminsections/view_user').'/'.$rec['id'];?>" class="btn btn-success"><i class="fa fa-eye ic" aria-hidden="true" title="Edit"></i> View</a>
                              
                                </td>
                            </tr>
                            <?php  $i++;endforeach; ?>
                        </tbody>
                    </table>
                </div>

    			</div>
    		</div>
    	</div>
    </div>
  </div>
</main>
<!-- Contents Ends -->
<?php $this->load->view('admin_panel/admin_footer');?>

<?php  $this->load->view('admin_panel/admin_includes'); ?>
<?php $this->load->view('admin_panel/admin_sidebar');?>
<!-- Contents -->
<?php
     $this->load->model('general_model','general');
     $ci = &get_instance();  
     $questions_count = $ci->general->get_questions_count();
     $questions = $ci->general->get_all_questions_for_listing();
     if (!(isset($this->session->userdata['admin_logged_in']))) {
    $user_id_login = 0; 
 }
 else{
  $sesn = $this->session->userdata['admin_logged_in'];   
  $user_id_login = $sesn['user_id'];   
 }

 $question_categories = $ci->general->get_question_categories($post_details->id);
 $user_details = $ci->general->profile($post_details->user_id);
 $view_question = $ci->general->get_count_of_view_question($post_details->id);
 $check_votes = $ci->general->check_votes_post($post_details->id,$user_id_login);
 $get_vote_count = $ci->general->get_vote_count_post($post_details->id);
 $count_answer = $ci->general->get_post_answer_count($post_details->id);
 $all_answers = $ci->general->get_all_post_answers($post_details->id);

  $menu=$ci->general->get_menu_admin($all_answers,0);
?>
<main class="l-main">
  <div class="content-wrapper content-wrapper--with-bg">
    <h3 class="page-title">Post View</h3>
    <div class="page-content">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
                <div class=" bx_qstn bx_qstn_odd">
                  <input type="hidden" id="login_user_id" value="<?php echo $user_id_login;?>">
                  <input type="hidden" id="question_id_for_vote" value="<?php echo $post_details->id;?>">
                <h6 class="q_title"><?php echo $post_details->post_title;?></h6>
                <p class="cmn_p"><?php echo $post_details->post_content;?></p>
                          <?php 
                                    if(!empty($question_categories)){
                                      foreach ($question_categories as $key => $q_cs) {
                                       $question_category_name = $ci->general->get_question_category_name($q_cs['category_id']);  
                                  ?>
                                           
                                                <span><a href="<?php echo base_url('Welcome/related').'/'.$q_cs['category_id'];?>" class="a_href_in_m_u_t"><?php echo $question_category_name;?></a></span>
                                            
                                  <?php  }
                                   } ?>
                                   <div class="row div_spc_qstns">
                          <div class="col-sm-1">
                                                    <?php 
                                                    if(isset($user_details->profile_picture)){
                                                      ?>
                                                      <img src="<?php echo base_url('assets/profiles').'/'.$user_details->profile_picture;?>" class="right_side_listing_image">
                                                      <?php
                                                    }
                                                    else{
                                                  ?>
                                                  <img src="<?php echo base_url() ?>assets/images/user.png" class="right_side_listing_image">
                                                  <?php } ?>
                                                </div>
                          <div class="col-sm-5">
                                                    <?php 
                                                    if($user_details->id == $user_id_login){
                                                      ?>
                                                      <a href="<?php echo base_url('Userhome/user_profile').'/'.$user_id_login;?>" class="cmn_a"><?php echo $user_details->full_name;?></a>

                                                      <?php
                                                    }
                                                    else{
                                                      ?>
                                                      <a href="<?php echo base_url('Welcome/user_profile_public').'/'.$user_details->id;?>" class="cmn_a"><?php echo $user_details->full_name;?></a>
                                                      <?php
                                                    }
                                                  ?>

                                                </div>
                          <div class="col-sm-6">
                            <p class="cmn_p"><i class="fa fa-info-circle" aria-hidden="true"></i> Posted on <?php echo $user_details->created_at;?></p>
                          </div>
                          <?php 
                          if (!(isset($this->session->userdata['admin_logged_in']))){
                            ?>
                            <a href="<?php echo base_url() ?>Welcome/login"><i class="fa fa-heart-o"></i></a>&nbsp;<span id="not_login"><?php echo $get_vote_count;?> Votes </span>
                            <?php
                           }
                           else{
                            if(empty($check_votes)){
                              ?>
                              <a href="#" id="check_vote_in" class="cmn_a"><i class="fa fa-heart-o"></i></a>&nbsp;<span id="voted"><?php echo $get_vote_count;?> Votes</span>
                              <?php
                            }
                            else{
                              ?>
                              <a href="#" id="check_vote_out" class="cmn_a"><i class="fa fa-heart"></i></a>&nbsp;<span id="remove_vote"><?php echo $get_vote_count;?> Votes</span>
                              <?php
                            }
                           }
                          ?>
                          <?php 
                             if (!(isset($this->session->userdata['admin_logged_in']))){
                            ?>
                            <span class="pl-2"><a href="<?php echo base_url() ?>Welcome/login" class="cmn_a">Comments(<?php echo $count_answer?>)</a></span>
                          <?php }
                          else{
                            ?>
                            <span class="pl-2"><a href="#" class="cmn_a">Comments(<?php echo $count_answer?>)</a></span>
                            <div class="comments_div">
                              <?php  echo form_open('#', ['class' => 'form-inline']); ?>
                              <textarea id="comment" name="comment" ></textarea><button type="button" class="btn btn-primary" id="save_answer">Submit</button>
                              <button type="button" class="btn btn-primary" id="update_answer">Update</button>
                              <?php echo form_close(); ?>
                              <div id="answers">
                                <input type="hidden" name="m_id" id="m_id" value="0">
                                <input type="hidden" name="post_id" id="post_id" value="<?php echo $post_details->id; ?>">
                                <input type="hidden" name="p_id" id="p_id" value="0">
                                 <?php echo $menu;?>
                              </div>
                            </div>
                            <?php
                          }
                          ?>
                          </span>
                        </div>
              </div>

    			</div>
    		</div>
    	</div>
    </div>
  </div>
</main>
<!-- Contents Ends -->
<?php $this->load->view('admin_panel/admin_footer');?>
<script>
$(document).ready(function(){
  $("#check_vote_in").click(function(){
    login_user_id = $("#login_user_id").val();
    question_id_for_vote = $("#question_id_for_vote").val();

     $.ajax({
            type: 'POST',
            url: '<?php echo base_url().'Adminsections/add_vote_post' ?>',
            data: {login_user_id:login_user_id,question_id_for_vote:question_id_for_vote},
            beforeSend: function() {
            },
            success: function(data) {

                if(data.result=='success')
                {

                   $("#voted").text(data.cnt+" Votes");
                    location.reload(true);
                }
                else
                {

//                    toastr.error("Error occured.please try again");

                }

            },
            error: function(xhr) { // if error occured
//                toastr.error("Error occured.please try again");

            },
            complete: function() {

            },
            dataType: 'json'
        });

  });
});
</script>
<script type="text/javascript">
  $("#check_vote_out").click(function(){
    login_user_id = $("#login_user_id").val();
    question_id_for_vote = $("#question_id_for_vote").val();

     $.ajax({
            type: 'POST',
            url: '<?php echo base_url().'Adminsections/remove_vote_post' ?>',
            data: {login_user_id:login_user_id,question_id_for_vote:question_id_for_vote},
            beforeSend: function() {
            },
            success: function(data) {

                if(data.result=='success')
                {

                   $("#remove_vote").text(data.cnt+" Votes");
                    location.reload(true);
                }
                else
                {

//                    toastr.error("Error occured.please try again");

                }

            },
            error: function(xhr) { // if error occured
//                toastr.error("Error occured.please try again");

            },
            complete: function() {

            },
            dataType: 'json'
        });

  });
</script>
<script type="text/javascript">
  $("#save_answer").click(function(){
    login_user_id = $("#login_user_id").val();
    comment = $("#comment").val();
    post_id = $("#post_id").val();
    p_id = $("#p_id").val();
     $.ajax({
            type: 'POST',
            url: '<?php echo base_url().'Adminsections/add_post_answer' ?>',
            data: {login_user_id:login_user_id,comment:comment,post_id:post_id,p_id:p_id},
            beforeSend: function() {
            },
            success: function(data) {

                if(data.result=='success')
                {
                 
                    location.reload(true);
                }
                else
                {

//                    toastr.error("Error occured.please try again");

                }

            },
            error: function(xhr) { // if error occured
//                toastr.error("Error occured.please try again");

            },
            complete: function() {

            },
            dataType: 'json'
        });

  });
</script>
<script type="text/javascript">
  function for_parent_id(id){
   $("#p_id").val(id);
   $("#comment").focus();
  }
</script>
<script type="text/javascript">
  function edit_cmnt(id){
    cmnt = $("#cmnt"+id).val();
    console.log(cmnt);
      $("#comment").val(cmnt);
      $("#m_id").val(id);
      $("#save_answer").css("display","none");
      $("#update_answer").css("display","block");  
     
  }
</script>
<script type="text/javascript">
  function remove_cmnt(id){
        if(confirm('Are you sure to remove this comment ?'))
        {
            $.ajax({
               type: 'POST',
               url: '<?php echo base_url().'Adminsections/remove_post_comment' ?>',
               data: {id: id},
               error: function() {
                  alert('Something is wrong');
               },
               success: function(data) {
                   
                    alert("Record removed successfully");  
                    location.reload(true);
               }
            });
        }
  }
</script>
<script type="text/javascript">
  $("#update_answer").click(function(){
    login_user_id = $("#login_user_id").val();
    comment = $("#comment").val();
    post_id = $("#post_id").val();
   
    m_id = $("#m_id").val();

     $.ajax({
            type: 'POST',
            url: '<?php echo base_url().'Adminsections/update_post_answer' ?>',
            data: {login_user_id:login_user_id,comment:comment,post_id:post_id,m_id:m_id},
            beforeSend: function() {
            },
            success: function(data) {

                if(data.result=='success')
                {
                 
                    location.reload(true);
                }
                else
                {

//                    toastr.error("Error occured.please try again");

                }

            },
            error: function(xhr) { // if error occured
//                toastr.error("Error occured.please try again");

            },
            complete: function() {

            },
            dataType: 'json'
        });

  });
</script>


<?php  $this->load->view('admin_panel/admin_includes'); ?>
<?php $this->load->view('admin_panel/admin_sidebar');?>
<!-- Contents -->
<?php
     $this->load->model('general_model','general');
     $ci = &get_instance();  
?>
<main class="l-main">
  <div class="content-wrapper content-wrapper--with-bg">
    <h3 class="page-title">Add Question   <a href="<?php echo base_url('Adminsections/questions'); ?>"><button type="button" class="btn btn-info btn_add">Question List</button></a></h3>
    <div class="page-content">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
                 <?php
                   if(isset($message)){
                    ?>
                    <div class="alert alert-success">
                        <strong><?php echo $message?></strong>
                    </div>
                    <?php 
                   }
                  ?>
    				<?php  echo form_open('Adminsections/add_question', ['id' => 'frmUsers','class' => '']); ?>
                  <div class="form-group">
                     <label>Question : </label>
             <textarea class="area_editor" id="contents" name="question"></textarea>
          </div> 

          <div class="form-group">
            <label>Categories : </label>
            <select id="category_id" name="category_id[]" class="select2 form-control" multiple data-live-search="true" data-placeholder="Choose Categories">
              
              <?php
                    foreach ($results as $key => $value) {
                      ?>
                      <option value="<?php echo $value['id'];?>"><?php echo $value['category_name_eng'];?></option>
                      <?php 
                    }
                   ?>
            </select>

          </div>
          <div class="form-group">
            <label>Language : </label>
            <select id="lan" name="lan" class="form-control">
              <option value="">Choose</option>
              <option value="0">English</option>
              <option value="1">Malayalam</option>
            </select>
          </div>
           <button type="submit" name="sbt" class="btn btn-primary">Add</button>
                 <?php echo form_close(); ?>

    			</div>
    		</div>
    	</div>
    </div>
  </div>
</main>
<!-- Contents Ends -->
<?php $this->load->view('admin_panel/admin_footer');?>


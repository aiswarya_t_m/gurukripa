<?php  $this->load->view('admin_panel/admin_includes'); ?>
<?php $this->load->view('admin_panel/admin_sidebar');?>
<!-- Contents -->
<main class="l-main">
  <div class="content-wrapper content-wrapper--with-bg">
    <h3 class="page-title">Add Package</h3>
    <div class="page-content">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
    				<span class="btn btn-primary btn_cmn"><a href="<?php echo base_url();?>Adminsections/packages" class="a_href_in_admin_panel"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;List Packages</a></span>
                </div>
                <div class="col-sm-12">

    				    <?php 
                  if(isset($message)){
                    ?>
                    <div class="alert alert-success">
                        <strong><?php echo $message?></strong>
                    </div>
                    <?php
                  }
                ?>
                <?php if((form_error('name')) != false) { ?>
                 <div class="alert alert-danger">
                  <strong><?php echo  form_error('description') ?></strong> 
                </div>
                 <div class="alert alert-danger">
                  <strong><?php echo  form_error('price') ?></strong> 
                </div>
             <?php } ?>
            
                 <?php  echo form_open('Adminsections/add_package', ['id' => 'frmUsers']); ?>
                  
                   <div class="form-group">
                    <label>Package Name</label>
                    <input type="text" class="form-element-l" autocomplete="off" name="pack_name" id="pack_name" placeholder="Package Name" value="<?php if(validation_errors() != false) echo set_value('pack_name'); ?>">
                  </div>
                 
                   <div class="form-group">
                    <label>Description</label>
                    <textarea  class="form-element-text-area pack_des" autocomplete="off" name="pack_des" id="pack_des" placeholder="Description"><?php if(validation_errors() != false) echo set_value('pack_name');?></textarea>
                  </div>

                   <div class="form-group">
                    <label>Price</label>
                    <input type="text" class="form-element-l" autocomplete="off" name="price" id="price" placeholder="Price" value="<?php if(validation_errors() != false) echo set_value('price'); ?>">
                  </div>

                   <button type="submit" name="sbt" class="btn btn-primary">Add</button>
                 <?php echo form_close(); ?>

    			</div>
    		</div>
    	</div>
    </div>
  </div>
</main>
<!-- Contents Ends -->
<?php $this->load->view('admin_panel/admin_footer');?>
<script>
    var editor = new Jodit('.pack_des', {
      "uploader": {
    
    "insertImageAsBase64URI": true
  }
    });
     
</script>
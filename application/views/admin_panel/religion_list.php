<?php  $this->load->view('admin_panel/admin_includes'); ?>
<?php $this->load->view('admin_panel/admin_sidebar');?>
<!-- Contents -->
<main class="l-main">
  <div class="content-wrapper content-wrapper--with-bg">
    <h3 class="page-title">Religion List</h3>
    <div class="page-content">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
    				<span class="btn btn-primary btn_cmn"><a href="<?php echo base_url();?>Adminsections/add_religion" class="a_href_in_admin_panel"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add New</a></span>

    				<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Religion</th>
                         
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;foreach($results as $rec):
                            ?>
                            <tr id="<?php echo $rec['id']; ?>">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $rec['name']; ?></td>
                              
                                <td>
                                <a href="<?php echo base_url('Adminsections/edit_religion').'/'.$rec['id'];?>" class="btn btn-success"><i class="fa fa-edit ic" aria-hidden="true" title="Edit"></i> Edit</a>
                                &nbsp;
                                <a class="btn btn-danger remove"><i class="fa fa-times-circle ic" aria-hidden="true" title="Delete"></i> Delete</a>
                                </td>
                            </tr>
                            <?php  $i++;endforeach; ?>
                        </tbody>
                    </table>
                </div>

    			</div>
    		</div>
    	</div>
    </div>
  </div>
</main>
<!-- Contents Ends -->
<?php $this->load->view('admin_panel/admin_footer');?>

<script type="text/javascript">
    $(".remove").click(function(){
        var id = $(this).parents("tr").attr("id");
        if(confirm('Are you sure to remove this record ?'))
        {
            $.ajax({
               type: 'POST',
               url: '<?php echo base_url().'Adminsections/remove_religion' ?>',
               data: {id: id},
               error: function() {
                  alert('Something is wrong');
               },
               success: function(data) {
                    $("#"+id).remove();
                    alert("Record removed successfully");  
               }
            });
        }
    });
</script> 
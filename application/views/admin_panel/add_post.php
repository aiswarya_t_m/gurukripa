<?php  $this->load->view('admin_panel/admin_includes'); ?>
<?php $this->load->view('admin_panel/admin_sidebar');?>
<!-- Contents -->
<?php
     $this->load->model('general_model','general');
     $ci = &get_instance();  
?>
<main class="l-main">
  <div class="content-wrapper content-wrapper--with-bg">
    <h3 class="page-title">Add Post   <a href="<?php echo base_url('Adminsections/posts'); ?>"><button type="button" class="btn btn-info btn_add">Post List</button></a></h3>
    <div class="page-content">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
                 <?php
                   if(isset($message)){
                    ?>
                    <div class="alert alert-success">
                        <strong><?php echo $message?></strong>
                    </div>
                    <?php 
                   }
                  ?>
    				<?php  echo form_open('Adminsections/add_post', ['id' => 'frmUsers','class' => '']); ?>
         <div class="form-group">
                     <label>Title : </label>
             <input type="text" name="post_title" id="post_title" class="form-control">
          </div> 
                  <div class="form-group">
                     <label>Content : </label>
             <textarea class="area_editor" id="contents" name="post_content"></textarea>
          </div> 

          <div class="form-group">
            <label>Categories : </label>
            <select id="category_id" name="category_id[]" class="select2 form-control" multiple data-live-search="true" data-placeholder="Choose Categories">
              
              <?php
                    foreach ($results as $key => $value) {
                      ?>
                      <option value="<?php echo $value['id'];?>"><?php echo $value['category_name_eng'];?></option>
                      <?php 
                    }
                   ?>
            </select>

          </div>
          <div class="form-group">
            <label>Language : </label>
            <select id="lan" name="lan" class="form-control">
              <option value="">Choose</option>
              <option value="0">English</option>
              <option value="1">Malayalam</option>
            </select>
          </div>
           <button type="submit" name="sbt" class="btn btn-primary">Add</button>
                 <?php echo form_close(); ?>

    			</div>
    		</div>
    	</div>
    </div>
  </div>
</main>
<!-- Contents Ends -->
<?php $this->load->view('admin_panel/admin_footer');?>


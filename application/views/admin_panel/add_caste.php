<?php  $this->load->view('admin_panel/admin_includes'); ?>
<?php $this->load->view('admin_panel/admin_sidebar');?>
<!-- Contents -->
<main class="l-main">
  <div class="content-wrapper content-wrapper--with-bg">
    <h3 class="page-title">Add Caste</h3>
    <div class="page-content">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
    				<span class="btn btn-primary btn_cmn"><a href="<?php echo base_url();?>Adminsections/caste" class="a_href_in_admin_panel"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;List Caste</a></span>
                </div>
                <div class="col-sm-12">

    				    <?php 
                  if(isset($message)){
                    ?>
                    <div class="alert alert-success">
                        <strong><?php echo $message?></strong>
                    </div>
                    <?php
                  }
                ?>
                <?php if((form_error('name')) != false) { ?>
                 <div class="alert alert-danger">
                  <strong><?php echo  form_error('name') ?></strong> 
                </div>
                 <div class="alert alert-danger">
                  <strong><?php echo  form_error('religion_id') ?></strong> 
                </div>
             <?php } ?>
            
                 <?php  echo form_open('Adminsections/add_caste', ['id' => 'frmUsers']); ?>
                  
                   <div class="form-group">
                    <label>Caste</label>
                    <input type="text" class="form-element-l" autocomplete="off" name="caste_name" id="caste_name" placeholder="Caste" value="<?php if(validation_errors() != false) echo set_value('name'); ?>">
                  </div>
                 
                  <div class="form-group">
                    <label>Religion</label>
                    <select class="form-element-l select2" name="religion_id">
                      <option value="">Select Religion</option>

                      <?php

                      if(!empty($religions)){

                        foreach ($religions as $key => $value) {
                        ?>

                        <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>

                      <?php } } ?>
                    </select>
                  </div>
                  
                   <button type="submit" name="sbt" class="btn btn-primary">Add</button>
                 <?php echo form_close(); ?>

    			</div>
    		</div>
    	</div>
    </div>
  </div>
</main>
<!-- Contents Ends -->
<?php $this->load->view('admin_panel/admin_footer');?>

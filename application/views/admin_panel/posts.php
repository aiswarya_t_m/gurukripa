<?php  $this->load->view('admin_panel/admin_includes'); ?>
<?php $this->load->view('admin_panel/admin_sidebar');?>
<!-- Contents -->
<?php
     $this->load->model('general_model','general');
     $ci = &get_instance();  
?>
<main class="l-main">
  <div class="content-wrapper content-wrapper--with-bg">
    <h3 class="page-title">Post List <a href="<?php echo base_url('Adminsections/add_post'); ?>"><button type="button" class="btn btn-info btn_add">Add New Post</button></a></h3>
    <div class="page-content">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
                 <?php
                   if(isset($message)){
                    ?>
                    <div class="alert alert-success">
                        <strong><?php echo $message?></strong>
                    </div>
                    <?php 
                   }
                  ?>
    				<div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Post</th>
                            <th>User</th>
                            <th>Created At</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;foreach($results as $rec):

                        $user_id = $rec['user_id'];
                        $user_name=$ci->general->get_user($user_id);
                            ?>
                            <tr id="<?php echo $rec['id']; ?>">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $rec['post_title']; ?></td>
                                <td><?php echo $user_name; ?></td>
                                <td><?php echo $rec['created_at']; ?></td>
                                <td>
                                <?php if($rec['admin_approve'] == 0){ ?>
                                <a href="<?php echo base_url('Adminsections/approve_post').'/'.$rec['id'];?>" class="btn btn-info"><i class="fa fa-check ic" aria-hidden="true" title="Approve"></i> Approve</a>
                            <?php } else { ?>
                                <a href="#" class="btn btn-info"><i class="fa fa-thumbs-up ic" aria-hidden="true" title="Approve"></i> Approved</a>
                            <?php } ?>
                            <a href="#" class="btn btn-danger remove"><i class="fa fa-trash ic" aria-hidden="true" title="Reject"></i> Reject</a>
                            <a href="<?php echo base_url('Adminsections/view_post').'/'.$rec['id'];?>" class="btn btn-warning"><i class="fa fa-eye ic" aria-hidden="true" title="View"></i> View</a>
                                </td>
                            </tr>
                            <?php  $i++;endforeach; ?>
                        </tbody>
                    </table>
                </div>

    			</div>
    		</div>
    	</div>
    </div>
  </div>
</main>
<!-- Contents Ends -->
<?php $this->load->view('admin_panel/admin_footer');?>

<script type="text/javascript">
    $(".remove").click(function(){
        var id = $(this).parents("tr").attr("id");
        if(confirm('Are you sure to remove this record ?'))
        {
            $.ajax({
               type: 'POST',
               url: '<?php echo base_url().'Adminsections/reject_post' ?>',
               data: {id: id},
               error: function() {
                  alert('Something is wrong');
               },
               success: function(data) {
                    $("#"+id).remove();
                    alert("Record removed successfully");  
               }
            });
        }
    });
</script> 
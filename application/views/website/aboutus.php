<?php  $this->load->view('website/includes'); ?>
<?php $this->load->view('website/header');?>
<a id="back2Top" title="Back to top" href="#">&#10148;</a>
<!-- Contents -->
<section class="cmn_section about_us">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h4>About Us</h4>
				<img src="<?php echo base_url() ?>assets/images/content.PNG" class="img-fluid">
				<p class="cmn_p">Gurukripa Matrimony is a pioneer in matrimonial matchmaking services committed to provide 360 degree solutions to all prospective Kerala brides and Kerala grooms. We believe in providing a secure and convenient matrimonial matchmaking experience to our customers. In this generation's fast life, an Indian based matrimonial website, have connected brides and grooms from all around the world to search their life partners. We are one of the fastest growing portals for creating successful marriages. We blend tradition with technology. By choosing Intimate Matrimony you will be served with right guidelines and assistance for your partner search from the desired target group. Your search for the perfect life partner ends here.</p>

			</div>
		</div>
	</div>
</section>
<!-- Contents Ends -->
<?php $this->load->view('website/footer');?>
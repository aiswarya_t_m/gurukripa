<section id="get_started">
  <div class="container text-center">
    <div class="row">
      <div class="col-sm-12">
       <h4>Your story is waiting to happen! <a href="<?php echo base_url() ?>"><button type="button" class="btn btn-primary">Get Started</button></a></h4>
      </div>
    </div>
  </div>
</section>
<section id="footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <p class="text-center p-1">Copyright 2019 © Gurukripa Matrimony. All Rights Reserved. </p>
      </div>
    </div>
  </div>
</section>
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/css/bootstrap/popper.min.js"></script>
<script src="<?php echo base_url() ?>assets/css/bootstrap/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/select2/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/build/jodit.min.js" ></script>


<script type="text/javascript">
  /*Scroll to top when arrow up clicked BEGIN*/
$(window).scroll(function() {
    var height = $(window).scrollTop();
    if (height > 100) {
        $('#back2Top').fadeIn();
    } else {
        $('#back2Top').fadeOut();
    }
});
$(document).ready(function() {
    $("#back2Top").click(function(event) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

});
</script>
<script type="text/javascript">
    $( document ).ready(function() {
       var toggleAffix = function(affixElement, scrollElement, wrapper) {
  
        var height = affixElement.outerHeight(),
            top = wrapper.offset().top;
        
        if (scrollElement.scrollTop() >= top){
            wrapper.height(height);
            affixElement.addClass("affix");
        }
        else {
            affixElement.removeClass("affix");
            wrapper.height('auto');
        }
          
      };
      

      $('[data-toggle="affix"]').each(function() {
        var ele = $(this),
            wrapper = $('<div></div>');
        
        ele.before(wrapper);
        $(window).on('scroll resize', function() {
            toggleAffix(ele, $(this), wrapper);
        });
        
        // init
        toggleAffix(ele, $(window), wrapper);
      });
      
    });
   
</script>

<script type="text/javascript">
    $(document).ready(function(){
     $(".select2").select2();
});
</script>
<script>
    var editor = new Jodit('.area_editor', {
       //
    });
     
</script>
<script type="text/javascript">
  $("#change_status").click(function(){
    login_user_id = $("#login_user_id").val();

     $.ajax({
            type: 'POST',
            url: '<?php echo base_url().'Userhome/remove_notification' ?>',
            data: {login_user_id:login_user_id},
            beforeSend: function() {
            },
            success: function(data) {

                if(data.result=='success')
                {

                  window.location ="<?php echo base_url().'Userhome/user_profile/' ?>"+login_user_id;
                }
                else
                {

//                    toastr.error("Error occured.please try again");

                }

            },
            error: function(xhr) { // if error occured
//                toastr.error("Error occured.please try again");

            },
            complete: function() {

            },
            dataType: 'json'
        });

  });
</script>
<script type="text/javascript">
    $("#category_change").change(function(){
      c_id = $("#category_change").val();
      window.location ="<?php echo base_url().'Welcome/related/' ?>"+c_id;
    });
</script>
<script type="text/javascript">
    $("#category_change_mal").change(function(){
      c_id = $("#category_change_mal").val();
      window.location ="<?php echo base_url().'Malayalam/related/' ?>"+c_id;
    });
</script>
<?php  $this->load->view('website/includes'); ?>
<?php $this->load->view('website/header');?>
<a id="back2Top" title="Back to top" href="#">&#10148;</a>
<!-- Contents -->
<section class="cmn_section login">
	<div class="container">
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<div class="login_div">
					<span class="reg"><i class="fa fa-pencil" aria-hidden="true"></i> Login</span><span class="free">NOW!</span>

					<?php 
                  if(isset($error_message)){
                  	?>
                  	<div class="alert alert-success">
  						<strong><?php echo $error_message?></strong>
					</div>
                  	<?php
                  }
				?>
					<?php  echo form_open('Welcome/login', ['id' => 'frmLogin','class' => '']); ?>
					<div class="form-group">
				    <input type="email" class="form-element" autocomplete="off" name="username" id="email" placeholder="Type Your Email Id">
				   </div>
				  <div class="form-group">
				    <input type="password" class="form-element" id="password" name="password" placeholder="Type Your Password">
				  </div>
				  <button type="submit" class="btn btn-primary btn_l"><i class="fa fa-key" aria-hidden="true"></i> LOGIN</button>

				  <div class="pt-3">
				  	If you don't have an account yet, <a class="cmn_a" href="<?php echo base_url() ?>">Register For Free</a>
				  </div>
					 <?php echo form_close(); ?>
				</div>
			</div>
			<div class="col-sm-3"></div>
		</div>
	</div>
</section>
<!-- Contents Ends -->
<?php $this->load->view('website/footer');?>
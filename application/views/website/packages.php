<?php  $this->load->view('website/includes'); ?>
<?php $this->load->view('website/header');?>
<a id="back2Top" title="Back to top" href="#">&#10148;</a>
<?php
$this->load->model('Admin_model','admin');
$ci = &get_instance();
$pakages=$ci->admin->package_list();
?>
<!-- Contents -->
<section class="cmn_section packages">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h4>Packages</h4>
			</div>
		</div>
		<div class="row spc">
		<?php 
			if(!empty($pakages)){
				foreach ($pakages as $key => $value) {
				?>
				<div class="col-sm-3">
				<div class="card">
				    <div class="card-header"><h3 class="text-center"><?php echo $value['price'];?></h3></div>
				    <div class="card-body card-body-pakages"><?php echo $value['description'];?></div> 
				    <div class="card-footer">
				    	<?php 
				    		if (!(isset($this->session->userdata['user_logged_in']))) {
  						  ?>
  						  <a href="<?php echo base_url() ?>Welcome/login" class="cmn_a"><button class="btn btn-primary cmn_btn">Pay Now</button></a>
  						  <?php
 							}
 							else{
 								?>
 								<a href="#" class="cmn_a"><button class="btn btn-primary cmn_btn">Pay Now</button></a>
 								<?php
 							}
				    	?>
				    </div>
				  </div>
			  </div>
				<?php
				}
			}
			?>
		</div>
		<div class="row spc">
			<div class="col-sm-12">
				<h4 class="t_p">Offline Payment</h4>
				<h6>Please mention your Profile ID while making the Payment</h6>
				<hr>
				<div class="row">
					<div class="col-sm-3">
						<div class="card">
						    <div class="card-header"><h6 class="text-center">STATE BANK OF INDIA</h6></div>
						    <div class="card-body card-body-pakages"></div> 
						    <div class="card-footer">
						    </div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="card">
						    <div class="card-header"><h6 class="text-center">INDIAN BANK</h6></div>
						    <div class="card-body card-body-pakages"></div> 
						    <div class="card-footer">
						    </div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="card">
						    <div class="card-header"><h6 class="text-center">LAKSHMIVILASAM BANK</h6></div>
						    <div class="card-body card-body-pakages"></div> 
						    <div class="card-footer">
						    </div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="card">
						    <div class="card-header"><h6 class="text-center">DD/MO</h6></div>
						    <div class="card-body card-body-pakages"></div> 
						    <div class="card-footer">
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</section>
<!-- Contents Ends -->
<?php $this->load->view('website/footer');?>
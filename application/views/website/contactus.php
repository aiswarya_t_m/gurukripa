<?php  $this->load->view('website/includes'); ?>
<?php $this->load->view('website/header');?>
<a id="back2Top" title="Back to top" href="#">&#10148;</a>
<!-- Contents -->
<section class="cmn_section about_us">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h4>Contact Us</h4>
				<hr>
				<div class="row">
					<div class="col-sm-5">
						<h5>GURUKRIPA  A Journey For Peace & Life</h5>
						<ul type="circle">
							<li>3rd Floor, Pottekkatt Building, </li>
							<li>Near Paramekkavu Temple, </li>
							<li>Round East, </li>
							<li>Thrissur - 680 001</li>
							<li>E-mail : gurukripatelevision@gmail.com   </li>
							<li>Phone : 9400124048, 9288603419</li>
						</ul>
					</div>
					<div class="col-sm-7">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3922.6982704925667!2d76.21473161428536!3d10.52441416667378!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba7ef05b864db69%3A0xf6a85d83850c6b55!2sGurukripa+A+Journey+For+Peace!5e0!3m2!1sen!2sin!4v1566023273588!5m2!1sen!2sin" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Contents Ends -->
<?php $this->load->view('website/footer');?>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="<?php echo base_url() ?>">GuruKripa</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() ?>"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() ?>Welcome/aboutus"><i class="fa fa-building" aria-hidden="true"></i> About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fa fa-search" aria-hidden="true"></i> Find Partner</a>
      </li>    
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() ?>Welcome/packages"><i class="fa fa-money" aria-hidden="true"></i> Packages</a>
      </li>    
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() ?>Welcome/contactus"><i class="fa fa-phone" aria-hidden="true"></i> Contact Us</a>
      </li>    
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() ?>Welcome/login"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url() ?>"><i class="fa fa-user-plus" aria-hidden="true"></i> Register</a>
      </li>
    </ul>
  </div>  
</nav>


<?php  $this->load->view('website/includes'); ?>
<?php $this->load->view('website/header');?>
<a id="back2Top" title="Back to top" href="#">&#10148;</a>
<!-- Banner -->
<style>
  .detail{list-style: none; font-weight: 700;}

</style>
<section id="banner">
	<div class="conatiner">
		<div class="row">
			<div class="col-sm-7"></div>
			<div class="col-sm-5">
				<div class="reg_card">
                                  <span class="reg"><i class="fa fa-pencil" aria-hidden="true"></i> Register</span><span class="free">FREE!</span>

                                  &nbsp&nbsp&nbsp&nbsp <span class="m-b-none text-left text-danger"><?php if(isset($error_msg)){  echo "* ". $error_msg; } ?></span>
					<?php  echo form_open('Welcome/quick_register', ['id' => 'frmUsers','class' => '']); ?>
					 <div class="row spc_row">
                                       <input type="hidden" class="s_name" name="s_name">
                                       <input type="hidden" class="religion_name" name="religion_name">
        
                                          <div class="col-sm-6">
                                                 <input type="text" name="full_name" id="" class="form-element" placeholder="Bride/Groom Name *">
                                          </div>
                                          <div class="col-sm-6">
                                                 <select class="form-element" name="prof_for">
                                                        <option value="">Profile For *</option>
                                                        <option value="Myself">Myself</option>
                                                        <option value="Son">Son</option>
                                                        <option value="Daughter">Daughter</option>
                                                        <option value="Brother">Brother</option>
                                                        <option value="Sister">Sister</option>
                                                        <option value="Friend">Friend</option>
                                                        <option value="Relative">Relative</option>
                                                        </select>
                                          </div>                              
                                    </div>
                                     <div class="row spc_row">
                                          <div class="col-sm-6 gender">
                                                 <div class="form-check-inline">
                                                   <label class="form-check-label">
                                                     <input type="radio" class="form-check-input" name="gender" value="Male">Male
                                                   </label>
                                                 </div>
                                                 <div class="form-check-inline">
                                                   <label class="form-check-label">
                                                     <input type="radio" class="form-check-input" name="gender" value="Female">Female 
                                                   </label>
                                                 </div>
                                          </div>
                                          <div class="col-sm-6">
                                                 <input type="text" name="dob" id="dob" class="form-element" placeholder="Date Of Birth *">
                                          </div>                              
                                    </div>
                                     <div class="row spc_row">
                                          <div class="col-sm-6">
                                                 <select class="form-element religion" name="">
                                                        <option value="">Religion *</option>
                                                        <?php
                                                        if(!empty($religion_list)){

                                                          foreach ($religion_list as $key => $value) {
                                                          ?>
                                                          <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                                                        <?php } } ?>
                                                        </select>
                                          </div>
                                          <div class="col-sm-6">
                                                 <select class="form-element caste" name="caste">
                                                        <option value="">Caste *</option>
                                                        
                                                        </select>
                                          </div>                              
                                    </div>
                                     <div class="row spc_row">
                                          <div class="col-sm-6">
                                                 <select class="form-element" name="country">
                                                        <option value="">Country *</option>
                                                        <option value="India">India</option>
                                                       
                                                        </select>
                                          </div>
                                          <div class="col-sm-6">
                                                 <select class="form-element states" name="state">
                                                        <option value="">State *</option>
                                                        <?php
                                                        if(!empty($states)){

                                                          foreach ($states as $key1 => $value1) {
                                                          ?>
                                                          <option value="<?php echo $value1['id'];?>"><?php echo $value1['name'];?></option>
                                                        <?php } } ?>
                                                        </select>
                                          </div>                              
                                    </div>
                                     <div class="row spc_row">
                                          <div class="col-sm-6">
                                                 <select class="form-element district" name="district">
                                                        <option value="">District *</option>
                                                        
                                                        </select>
                                          </div>
                                          <div class="col-sm-6">
                                                 <input type="text" name="mob_no" id="" class="form-element" placeholder="Mobile No *">
                                          </div>                              
                                    </div>
                                     <div class="row spc_row">
                                          <div class="col-sm-6">
                                                 <input type="email" name="email_id" id="" class="form-element" placeholder="Email Id *">
                                          </div>
                                          <div class="col-sm-6">
                                                 <input type="Password" name="password" id="" class="form-element" placeholder="Password *">
                                          </div>                              
                                    </div>
                                     <div class="row spc_row">
                                          <div class="col-sm-12"> <button type="submit" name="sbt" class="btn btn-primary">Register</button></div>
                                          <span class="m-b-none text-left text-success"><?php if(isset($message)){  echo $message; } ?></span>                            
                                    </div>               
					 <?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Banner Ends -->
<!-- Search -->
<section id="search_partner" class="cmn_section">
       <div class="container">
              <div class="row">
                     <div class="col-sm-4"></div>
                     <div class="col-sm-4"><h4 class="text-center hdng">Find Your Perfect Partner </h4></div>
                     <div class="col-sm-4"></div>
              </div>
              <div class="home_search_div spc">
              <?php  echo form_open('Userhome/add_post', ['id' => 'frmUsers','class' => '']); ?>
              <div class="row">
                <div class="col-sm-3">
                  <select class="form-element">
                  <option value="">Select Bride/Groom</option>
                  <option value="Female">Bride</option>
                  <option value="Male">Groom</option>
                  </select>
                </div>
                <div class="col-sm-3">
                  <select class="form-element">
                  <option value="">From Age</option>
                  <?php
                    for($i=18;$i<=70;$i++){
                      ?>
                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                      <?php
                    }
                   ?>
                  </select>
                </div>
                <div class="col-sm-3">
                  <select class="form-element">
                  <option value="">To Age</option>
                  <?php
                    for($i=18;$i<=70;$i++){
                      ?>
                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                      <?php
                    }
                   ?>
                  </select>
                </div>
                <div class="col-sm-3">
                  <select class="form-element">
                  <option value="">Choose Marital Status</option>
                  <option value="Divorced">Divorced</option>
                  <option value="Married">Married</option>
                  <option value="Seperated">Seperated</option>
                  <option value="Single">Single</option>
                  <option value="Widowed">Widowed</option>
                  </select>
                </div>
              </div>
              <div class="row spc_row">
                <div class="col-sm-3">
                  <select class="form-element religion">
                  <option value="">Choose Religion</option>

                   <?php
                  if(!empty($religion_list)){

                    foreach ($religion_list as $key => $value) {
                    ?>
                    <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                  <?php } } ?>

                  </select>
                </div>
                <div class="col-sm-3">
                  <select class="form-element caste">
                 
                  </select>
                </div>
                <div class="col-sm-3"><button type="button" class="btn btn-primary cmn_btn">SEARCH  <i class="fa fa-search" aria-hidden="true"></i></button></div>
                <div class="col-sm-3"></div>
              </div>
              <?php echo form_close(); ?>
            </div>
       </div>
</section>
<!-- Search Ends -->
<!-- Featured Lists -->
<div class="container">
   <div class="row spc">
                     <div class="col-sm-4"></div>
                     <div class="col-sm-4"><h4 class="text-center hdng">Featured Profiles </h4></div>
                     <div class="col-sm-4"></div>
              </div>
</div>
<div class="container cta-100 ">
    
          <div class="row blog">
            <div class="col-md-12">
              <div id="blogCarousel" class="carousel slide container-blog" data-ride="carousel">
                <ol class="carousel-indicators">
                  <?php
                  $count = 0;
                  foreach ($featured_list as $key => $value) {
                  if($count%3==0){ ?>
                  <li data-target="#blogCarousel" data-slide-to="<?php echo $count;?>" class="<?php if($count==0){ ?> active <?php } ?>"></li>
                  <?php
                }
                $count++;
              }
              ?>

                  
                </ol>
                <!-- Carousel items -->
                <div class="carousel-inner">

                  <?php
                  $count = 0;
                  $tot = count($featured_list);

                  if(!empty($featured_list))
                 {

                  foreach ($featured_list as $key => $value) {

                    $dateOfBirth = $value['dob'];
                    $today = date("Y-m-d");
                    $diff = date_diff(date_create($dateOfBirth), date_create($today));
                    $age = $diff->format('%y');
                   
                   if($count%3==0){ ?>


                  <div class="carousel-item <?php if($count==0){ ?> active <?php } ?>">
                    <div class="row">

                    <?php } ?>

                      <div class="col-md-4" >
                        <div class="item-box-blog">
                          <div class="item-box-blog-image">
                            <!--Date-->
                            <div class="item-box-blog-date bg-blue-ui white"> <span class="mon"><?php echo date("Y-M-d",strtotime($value['created_at']));?></span> </div>
                            <!--Image-->
                            <figure> <img alt="" src="https://cdn.pixabay.com/photo/2017/02/08/14/25/computer-2048983_960_720.jpg"> </figure>
                          </div>
                          <div class="item-box-blog-body">
                            <!--Heading-->
                            <div class="item-box-blog-heading">
                              <a href="#" tabindex="0">
                                <h5><?php echo $value['full_name'];?></h5>
                              </a>
                            </div>
                            <!--Data-->
                            <div class="item-box-blog-data" style="padding: px 15px; font-weight: 700;">

                               <p><?php echo $value['web_id'];?> , <?php echo $age; ?> Yrs,   <?php echo $value['height'];?> Cm</p>
                             <!--  <p><i class="fa fa-user-o"></i> Admin, <i class="fa fa-comments-o"></i> Comments(3)</p> -->
                            </div>
                            <!--Text-->
                            <div class="item-box-blog-text">

                              
          
      
                                  <li class="detail">
                                
                                    <p>Religion  : <?php echo $value['religion'];?></p>
                                    <p>Caste  : <?php echo $value['caste'];?></p>
                                    <p>Location  : <?php echo $value['district'];?></p>
                                   
                                 </li>
                              <!-- <p>Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet, consectetuer adipiscing. Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet, adipiscing. Lorem ipsum dolor sit amet, consectetuer adipiscing. Lorem ipsum dolor.</p> -->
                            </div>
                            <div class="mt"> <a href="#" tabindex="0" class="btn bg-blue-ui white read">read more</a> </div>
                            <!--Read More Button-->
                          </div>
                        </div>
                      </div>
                      <?php 

                      if($count%3==2 || $tot-1 == $count){
                      ?>
                       </div>
                    <!--.row-->
                  </div>
                  <?php
                }
                $count++;
              }
            }
            ?>
                      



                   
                  <!--.item-->
                  


                  <!--.item-->
                </div>
                <!--.carousel-inner-->
              </div>
              <!--.Carousel-->
            </div>
          </div>
    
      </div>
<!-- Featured Lists Ends -->
<!-- Find Your Soulmate -->
<section class="cmn_section f_y_s">
  <div class="container">
    <div class="row row_cmn">
         <div class="col-sm-4"></div>
         <div class="col-sm-4"><h4 class="text-center hdng">Find Your Soulmate </h4></div>
         <div class="col-sm-4"></div>
  </div>
    <div class="row ">
      <div class="col-sm-4">
          <div class="card bg-secondary text-white">
            <div class="card-body text-center">
              <i class="fa fa-user-plus fa-lg"></i>
              <h3>Signup</h3>
              <h6>Signup for free and Upload your profile</h6>
            </div>
          </div>
      </div>
      <div class="col-sm-4">
          <div class="card bg-secondary text-white">
            <div class="card-body text-center">
              <i class="fa fa-search fa-lg"></i>
              <h3>Search</h3>
              <h6>Search for your right partner</h6>
            </div>
          </div>
      </div>
      <div class="col-sm-4">
          <div class="card bg-secondary text-white">
          <div class="card-body text-center">
              <i class="fa fa-group fa-lg"></i>
              <h3>Connect</h3>
              <h6>Connect your perfect Match</h6>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Find Your Soulmate Ends  -->
<?php $this->load->view('website/footer');?>
<script>
  
$("#dob").datepicker();

$(".states").change(function(){
  states_val = $(this).val();

 var csrf_token = '<?php echo $this->security->get_csrf_hash(); ?>';

  
  $(".s_name").val($(".states option:selected").text());
  $.ajax({
            type: 'POST',
            url: '<?php echo base_url().'Welcome/get_district' ?>',
            data: {states_val:states_val, csrf_test_name: csrf_token},
            beforeSend: function() {



            },
            success: function(data) {


                if(data.result=='success')
                {

                   $(".district").html(data.drpdwn);
                  
                }
                else
                {

//                    toastr.error("Error occured.please try again");


                }



            },
         
            dataType: 'json'
        });
});

$(".religion").change(function(){
  rel_val = $(this).val();

 var csrf_token = '<?php echo $this->security->get_csrf_hash(); ?>';

  
  $(".religion_name").val($(".religion option:selected").text());
  $.ajax({
            type: 'POST',
            url: '<?php echo base_url().'Welcome/get_caste' ?>',
            data: {rel_val:rel_val, csrf_test_name: csrf_token},
            beforeSend: function() {



            },
            success: function(data) {


                if(data.result=='success')
                {

                   $(".caste").html(data.drpdwn);
                  
                }
                else
                {

//                    toastr.error("Error occured.please try again");


                }



            },
         
            dataType: 'json'
        });
});


</script>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Gurukripa</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/fav_icon.png" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap/bootstrap.min.css">
     <link rel="stylesheet" href="<?php echo base_url() ?>assets/select2/select2.min.css">
   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/animate.css" type="text/css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" type="text/css" media="all" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/datatables.min.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/build/jodit.min.css">
    
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">

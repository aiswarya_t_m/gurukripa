<?php  $this->load->view('website/includes'); ?>
<?php $this->load->view('website/header');?>
<a id="back2Top" title="Back to top" href="#">&#10148;</a>
<!-- Contents -->
<section class="cmn_section search_pf">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<div class="first_block">
				 <div class="row">
				 	<div class="col-sm-4">
				 		<img src="<?php echo base_url() ?>assets/images/1.jpg" class="img-thumbnail img-fluid">
				 	</div>
				 	<div class="col-sm-8">
				 		<h5 class="t_p">PROFILE ID : CSD57DG</h5>
				 		<hr>
				 		<h6 class="p_d">25 Years, 5' 8" , Christian, Protestant, MCA/PGDCA, Rs. 15 - 20 Lakh, Software.</h6>
				 		<button type="button" class="btn btn_intrst"><i class="fa fa-heart" aria-hidden="true"></i> Send Interest</button>
				 	</div>
				 </div>				
				</div>
				<div class="second_block">
					<h5 class="t_p">ABOUT ME</h5>
					<hr>
					<p class="para">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.

when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.

It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</p>
					<div class="table-responsive">
						<table class="table table-dark table-hover">
							<tbody>
						      <tr>
						        <td>Father : xxxx, Doctor</td>
						        <td>Religion : ---------------</td>
						        <td>Current Place  : ---------------</td>
						      </tr>
						      <tr>
						        <td>Mother : xxxx, Housewife</td>
						        <td>Caste : ---------------</td>
						        <td>Qualification : ---------------</td>
						      </tr>
						      <tr>
						        <td>Family Values : Moderate</td>
						        <td>Family Status : -------</td>
						        <td>Marital Status : -------</td>
						      </tr>
						      <tr>
						        <td>Designation : ---------------</td>
						        <td>Monthly Income : 1522000.00</td>
						        <td>Complexion : ---------------</td>
						      </tr>
						      <tr>
						        <td>Address : ----(country,state,district included)</td>
						        <td>Height : -----</td>
						        <td>Weight : -------</td>
						      </tr>
						      <tr>
						        <td>Health Problems : -----</td>
						        <td>Eating Habit : ---------------</td>
						        <td>Start Dhasha : ---------------</td>
						      </tr>
						      <tr>
						        <td>Married Brothers : -----</td>
						        <td>Married Sisters : ---------------</td>
						        <td>Unmarried Brothers : ---------------</td>
						      </tr>
						      <tr>
						        <td>Unmarried Sisters : -----</td>
						        <td>Type Of Horoscope : ---------------</td>
						        <td>Profile For : ------</td>
						      </tr>
						    </tbody>
						</table>
					</div>
				</div>	
			</div>
			<div class="col-sm-3">
				<div class="third_block">
					<h5 class="t_p">Search by Profile ID:</h5>
					<?php  echo form_open('Userhome/add_post', ['id' => 'frmUsers','class' => '']); ?>
					  <input type="text" name="web_id" id="web_id" class="form-element" placeholder="Type Profile Id">
					  <button type="button" class="btn btn-primary">Search</button>
					<?php echo form_close(); ?>
					<div class="other_profiles">
						<h5 class="t_p">Other Profiles</h5>
						<hr>
						<a href="#" class="cmn_a">
						<div class="row">
							<div class="col-sm-5">
								<img src="<?php echo base_url() ?>assets/images/1.jpg" class="img-fluid">
							</div>
							<div class="col-sm-7">
								<h6 class="t_p">ID : 235658</h6>
								<p class="p_o">29 Yrs, 5Ft 5in Christian
				  MBA/PGDM,
				  Rs 10 - 15 lac Mark...</p>
							</div>
						</div>
					   </a>
					   <hr>
					   <a href="#" class="cmn_a">
						<div class="row">
							<div class="col-sm-5">
								<img src="<?php echo base_url() ?>assets/images/1.jpg" class="img-fluid">
							</div>
							<div class="col-sm-7">
								<h6 class="t_p">ID : 989898</h6>
								<p class="p_o">30 Yrs, 5Ft 5in Christian
				  MBA/PGDM,
				  Rs 10 - 15 lac Mark...</p>
							</div>
						</div>
					   </a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Contents Ends -->
<?php $this->load->view('website/footer');?>